// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

const axios = require("axios");
export default async function handler(req, res) {
    let data = `https://api-suc.hubba.dev/rejection-reason/search`
    let teste = await axios.post(data, {
        "limit": 200,
        "term": ""
    }).catch(error => { return [{ erro: true, message: `Erro na requisição ${data}` }] });
    teste = teste.data.body.list.map(temp => ({
            id: temp.id,
            motivo: temp.motivo,
            ativo: temp.ativo == 1 ? "Ativo" : "Inativo",
            icon: "editIcon",
            trash: "trashIcon",
            to: "/hubba/cadastros/motivosRejeicao",
            className: "unselected"
    }))
    res.status(200).json(teste);
}
