// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
const dataTable = require("../../variables/pedidos/dataTable.json");
export default function handler(req, res) {
  let json = JSON.parse(req.body);
  if (json.type.includes("LIST")) {
    res.status(200).json(dataTable);
  }
}
