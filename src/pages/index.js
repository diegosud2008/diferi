import { Divider } from 'antd';
import Router from 'next/router'
import React, { useEffect } from 'react';
import { readPage } from '../controller/pages';

export default function Home() 
{
  React.useEffect(() => {
    Router.push(readPage().to);
    
  },[])
  
    return (
      <div style={{display: 'flex',alignItems: 'center',justifyContent: 'center', height: '100vh'}}><img src="https://www.blogson.com.br/wp-content/uploads/2017/10/d9933c4e2c272f33b74ef18cdf11a7d5.gif" /></div>
    )

}
