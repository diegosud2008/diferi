import React from "react";
import Themes from "../../../components/themes/default";
import { Configs } from "../../../components/view/configuracoes/configs";
const MenuJson = require("../../../variables/default/menuDefault.json");
export default function manutencao() {
  return <Themes Components={() => <Configs />} MenuJson={MenuJson} />; 
}
