import React from "react";
import Themes from "../../../components/themes/default";
import  { AjusteConfig } from "../../../components/view/configuracoes/ajuste";
const MenuJson = require("../../../variables/default/menuDefault.json");
export default function manutencao() {
  return <Themes Components={() => <AjusteConfig />} MenuJson={MenuJson} />;
}
