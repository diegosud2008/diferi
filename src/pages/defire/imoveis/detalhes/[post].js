import { useRouter } from "next/router";
import React from "react";
import Detalhes from "../../../../components/view/imoveis/detalhes";
import Themes from "../../../../components/themes/default";
const MenuJson = require("../../../../variables/default/menuDefault.json");

const Post = () => {
  const routes = useRouter();
  const { post } = routes.query;

  return (
    <Themes Components={() => <Detalhes props={post} />} MenuJson={MenuJson} />
  );
};
export default Post;
