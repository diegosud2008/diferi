import { useRouter } from "next/router";
import React from "react";
import Edit from "../../../../components/view/imoveis/edit";
import Themes from "../../../../components/themes/default";
const MenuJson = require("../../../../variables/default/menuDefault.json");

const Post = () => {
  const routes = useRouter();
  const { post } = routes.query;

  return (
    <Themes Components={() => <Edit props={post} />} MenuJson={MenuJson} />
  );
};
export default Post;
