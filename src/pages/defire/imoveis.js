import React from "react";
import Themes from "../../components/themes/default";
import Imoveis from "../../components/view/imoveis/default";
const MenuJson = require("../../variables/default/menuDefault.json");
export default function imoveis() {
  return <Themes Components={() => <Imoveis />} MenuJson={MenuJson} />;
}
