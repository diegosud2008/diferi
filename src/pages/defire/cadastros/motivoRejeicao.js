import React from "react";
import Themes from "../../../components/themes/default";
import { MotivoRejeicao} from "../../../components/view/cadastros/motivoRejeicao";
const MenuJson = require("../../../variables/default/menuDefault.json");
export default function categorias() {
  return <Themes Components={() => <MotivoRejeicao />} MenuJson={MenuJson} />
}