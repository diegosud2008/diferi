import React from "react";
import Themes from "../../../components/themes/default";
import VinculoCategorias from "../../../components/view/cadastros/vinculosCategorias";
const MenuJson = require("../../../variables/default/menuDefault.json");
export default function vinculo() {
  return (
    <Themes Components={() => <VinculoCategorias />} MenuJson={MenuJson} />
  );
}
