import React from "react";
import Themes from "../../../components/themes/default";
import Categorias from "../../../components/view/cadastros/categorias";
const MenuJson = require("../../../variables/default/menuDefault.json");
export default function categorias() {
  return <Themes Components={() => <Categorias />} MenuJson={MenuJson} />
}