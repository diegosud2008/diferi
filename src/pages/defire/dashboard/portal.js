import React from "react";
import Themes from "../../../components/themes/default";
import DashboardPortal from "../../../components/view/dashboard/DashboardPortal";
const MenuJson = require("../../../variables/default/menuDefault.json");
export default function pedidos() {
  return (
    <Themes Components={() => <DashboardPortal />} MenuJson={MenuJson} />
  );
}
