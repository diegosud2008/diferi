import React from "react";
import Themes from "../../../components/themes/default";
import DashboardConector from "../../../components/view/dashboard/DashboardConector";
const MenuJson = require("../../../variables/default/menuDefault.json");
export default function pedidos() {
  return (
    <Themes Components={() => <DashboardConector />} MenuJson={MenuJson} />
  );
}
