import React from "react";
import Themes from "../../../components/themes/default";
import { Manutencao } from "../../../components/view/manutencao/prazoentrega";
const MenuJson = require("../../../variables/default/menuDefault.json");
export default function manutencao() {
  return <Themes Components={() => <Manutencao />} MenuJson={MenuJson} />;
}
