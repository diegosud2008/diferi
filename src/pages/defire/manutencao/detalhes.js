import React from "react";
import Themes from "../../../components/themes/default";
import  {Detalhes}  from "../../../components/view/manutencao/manutencao-detalhes";
const MenuJson = require("../../../variables/default/menuDefault.json");
export default function manutencao_detalhes() {
  return <Themes Components={() => <Detalhes />} MenuJson={MenuJson} />;
}
