import React from "react";
import Themes from "../../components/themes/default";
import Pedidos from "../../components/view/pedidos/default";
const MenuJson = require("../../variables/default/menuDefault.json");
export default function pedidos() {
  return <Themes Components={() => <Pedidos />} MenuJson={MenuJson} />;
}
