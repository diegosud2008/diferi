import React from "react";
import Themes from "../../components/themes/default";
import Publicacoes from "../../components/view/publicacoes/default";
const MenuJson = require("../../variables/default/menuDefault.json");
export default function publicacoes() {
  return <Themes Components={() => <Publicacoes />} MenuJson={MenuJson} />;
}
