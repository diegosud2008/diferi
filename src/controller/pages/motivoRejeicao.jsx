export async function TableData() {
    let dados = await fetch(`/api/pedidos`, {
      method: "POST",
      body: JSON.stringify({ type: "LIST" }),
    }).then((result) => result.json());
    return dados;
  }
  