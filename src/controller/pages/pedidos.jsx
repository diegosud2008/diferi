export async function TableData() {
  let dados = await fetch(`/api/motivosRejeicao`, {
    method: "POST",
    body: JSON.stringify({ type: "LIST" }),
  }).then((result) => result.json());
  console.log("dados", dados);
  return dados;
}
