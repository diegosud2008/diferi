export function savePage(page = {
    key: "1",
    to: "/",
    item: "Resumo"    
}) 
{
    localStorage.setItem("page",JSON.stringify(page));
}

export function readPage() 
{
    const page = localStorage.page ? JSON.parse(localStorage.page) : {key: "1",to: "/",item: "Resumo"};
    return page;
}