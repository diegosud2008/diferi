import styled from "styled-components";

const Wrapper = styled.div`
transition: 0.5s;
display: flex;
height: 100%;
.ant-table-row{
  height: 60px;
}
.ant-table-cell{
  padding: 0px;
}
.ant-table-content {
  /* margin: 160px 0; */
}
.icons {
  padding:5px;
  display: none;
  justify-content: flex-start;
  cursor: pointer;
  transform: 0.5s;
  width: 43px;
}

tr.active {
  background: #F3F5F9;
}
.align{
  transition: 0.5s;
  display: flex;
  justify-content: center;    
}
// tr.icons-view:hover .align {
//   width: 63px;
//   animation: preco 0.5s;
//   @keyframes preco {
//     from { width: 43px; }
//     to   {width: 63px}
//   }
// }
tr.icons-view:hover .icons  {
    display: flex;
    margin-left: 15px;
    animation: icons 0.5s;

  @keyframes icons {
        from { width: 0; }
        to   {width: 43px}
    }
}

.td-bolder{
  font-weight: bolder;
}
.site-drawer-render-in-current-wrapper {
  position: relative;
  width: 30%;
  text-align: center;
  border-top: 1px solid #ebedf0;
  border-radius: 2px;

}
.d-none {
  display: none;
}
.d-block {
  display: block !important;
  margin-top: 80px;
  height: calc(100vh - 80px);
}
.tamanho_70 {
  width: 70%;
  /* margin-right:16px; */
  transition: 0.2s;
  .all{
    width: 57%;
  }
}
.tamanho_100 {
  width: 100%;
  margin-right:32px;
  transition: 0.2s;
}
.imagem {
  display: flex;
  width: 100%;
  border-radius: 5px;
  padding: 4px 8px;
  img {
    display: flex;
    width: 100%;
    border-radius: 4px;
  }
}
.bloco {
  display: flex;
  flex-wrap: wrap;

  span {
    margin-right: 7px;
    margin-top: 4px;
    border: 1px dashed #010101;
    padding: 0px 8px;
    border-radius: 7px;
  }
  .dados-proprietario {
      border: none;
      span{
        background: #404056;
        color: #fff;
        border-radius: 5px;
        border: 0;
      }
  }
}
span.ant-input-affix-wrapper.input {
  margin-right: 22px;
}
div.escIcon {
  svg {
    margin: 8px 0 0 0; 
  }
}
}
.filterIconProd{
  margin-right: 24px;
}
.ant-table-wrapper{
  margin-top: 157px;
}
`;

export default Wrapper;
