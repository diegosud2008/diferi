import styled from "styled-components";

const Wrapper = styled.div`
  transition: 0.5s;
  display: flex;
  height: 100%;
  .ant-table-row{
   height: 60px !important;
   min-height: 60px !important;
   max-height: 60px !important;
  }
  .ant-table-cell{
    padding: 0px 5px;
  }
  .icons {
    padding: 2px;
    display: none;
    justify-content: flex-start;
    cursor: pointer;
    transform: 0.5s;
  }
  .imgs {
    width: 100%;
  }
  .dot {
  height: 0.7rem;
  width: 0.5rem;
  background-color: rgb(248, 7, 7);
  border-radius: 50%;
  display: inline-block;
  }
  tr.active {
    background: #f3f5f9;
  }
  tr.icons-view:hover .icons {
    display: flex;
    animation: icons 0.5s;

    @keyframes icons {
      from {
        width: 0;
      }
      to {
        width: 43px;
      }
    }
  }
  .align {
    transition: 0.5s;
  }
  .icons-view:hover .align {
    width: 63px;
    animation: preco 0.5s;
    @keyframes preco {
      from {
        width: 43px;
      }
      to {
        width: 63px;
      }
    }
  }
  .site-drawer-render-in-current-wrapper {
    position: relative;
    width: 30%;
    overflow: hidden;
    text-align: center;
    border-top: 1px solid #ebedf0;
    border-radius: 2px;
  }
  .d-none {
    display: none;
  }
  .d-block {
    display: block !important;
  }
  .tamanho_70 {
    width: 70%;
    transition: 0.2s;
    .all{
      width: 58%;
    }
  }
  .tamanho_100 {
    width: 100%;
    transition: 0.2s;
  }
  .iconMargin {
    margin-right: 10px;
  }
  .td-bolder {
    font-family: Open Sans;
    font-style: normal;
    font-weight: bold;
    font-size: 14px;
    line-height: 19px;
    text-align: right;
    color: #2A2A2C;
  }
  .td-opaccity{
    font-family: Open Sans;
    font-style: normal;
    font-weight: normal;
    font-size: 12.5px;
    line-height: 17px;
    text-align: right;
    color: #2A2A2C;
    opacity: 0.5;
  }
`;
export default Wrapper;
