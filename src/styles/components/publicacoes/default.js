import styled from "styled-components";

const Wrapper = styled.div`
transition: 0.5s;
display: flex;
height: 100%;
.ant-table-cell{
  padding:0px;
}
.ant-table-content {
  margin: 160px 0;
}
.imgs{
  width:72px;
  height:72px;
}
.dot{
  width:8px;
  height: 28px;
  border-radius:4px;
}
tr.active {
  background: #F3F5F9;
}
tr.icons-view:hover .icons  {
  display: flex;
  animation: icons 0.5s;

  @keyframes icons {
        from { width: 0; }
        to   {width: 43px}
    }
}
.align{
transition: 0.5s;
}
.icons-view:hover .align {
width: 63px;
animation: preco 0.5s;
@keyframes preco {
  from { width: 43px; }
  to   {width: 63px}
}
}
.td-bolder{
  font-weight: bolder;
}
.site-drawer-render-in-current-wrapper {
  position: relative;
  width: 30%;
  text-align: center;
  border-top: 1px solid #ebedf0;
  border-radius: 2px;
}
.d-none {
  display: none;
}
.d-block {
  display: block !important;
  margin-top: 80px;
  height: calc(100vh - 80px);
}
.tamanho_70 {
  width: 70%;
  /* margin-right:16px; */
  transition: 0.2s;
  .all{
    width: 57%;
  }
}
.tamanho_100 {
  width: 100%;
  margin-right:32px;
  transition: 0.2s;
}
.tableDefault {
  p:last-child {
    font-weight: bolder;
  }
}
.iconMargin{
  margin-right: 10px;
}
`;

export default Wrapper;
