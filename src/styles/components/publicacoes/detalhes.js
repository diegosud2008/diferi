import styled from "styled-components";

const Wrapper = styled.div`
.ant-collapse-header{
  width: 80vw !important;
}
.ant-table-row{
  height: 60px !important;
}
tr.active {
  background: #F3F5F9;
}
.align{
  transition: 0.5s;
  display: flex;
  justify-content: center;    
}
.sem-estoque{
  color:#F04A4A;
  font-family: Open Sans;
  font-style: normal;
  font-weight: bold;
  font-size: 12.5px;
  line-height: 17px;
}
.pausado{
  color:#E1B30F;
  font-family: Open Sans;
  font-style: normal;
  font-weight: bold;
  font-size: 12.5px;
  line-height: 17px;
}
`;

export default Wrapper;
