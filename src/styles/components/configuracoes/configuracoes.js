import styled from 'styled-components';

const Wrapper = styled.div`
.container { 
    .ant-table-content {
    margin: 162px 0;
    }
    svg {
        width: 20px !important;
    }
    button.buttonActive { 
        background-color: rgb(0, 134, 255);
        border: none;
        border-radius: 3px;
        color: #FFF;
        padding: 5px 10px;
        cursor: pointer;
    }
    button.btnEnabled {
        background-color: rgb(0, 134, 255)!important;
        border: none;
        border-radius: 3px;
        color: #FFF;
        padding: 5px 10px;
        cursor: pointer;
        animation: cor1 0.5s;
        transition:  0.8s;

        @keyframes cor1 {
            from{background-color: #CCC}
            to{background-color: rgb(0, 134, 255)}
        }
    }
    button.disabled {
        background-color: #CCC;
        border: none;
        border-radius: 3px;
        color: #FFF;
        padding: 5px 10px;
        opacity: 0.5;
        pointer-events: none;
        animation: cor 0.5s;

        @keyframes cor {
            from{background-color: rgb(0, 134, 255)}
            to{background-color: #CCC;}
        }
    }
}

`;

export default Wrapper;