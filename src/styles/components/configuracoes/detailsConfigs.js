import styled from "styled-components";

const Wrapper = styled.div`
  .number-align {
    text-align: end;
  }
  .headerDetalhesProdutos {
}

  .link-text {
    color: #0092e4 !important;
  }
  .ant-collapse-header {
    position: relative;
    border-bottom: 1px dashed #cecece;
    position: relative;
    padding-left: 0px !important;
    padding-right: 24px !important;
    padding-bottom: 6.5px !important;
    align-items: center !important;
    .ant-collapse-arrow {
      right: 0px !important;
    }
    svg {
      color: #4466ab;
    }
  }
  .default-button {
    color: #346698;
    cursor: default;
    font-family: Open Sans;
    font-style: normal;
    font-weight: bold;
    font-size: 14px;
    line-height: 19px;

    :hover {
      color: #346698;
    }
  }
  .link-style-div {
    align-items: center;
    .ant-form-item-control-input-content {
      display: flex;
    }
  }
  .link-style {
    margin-left: 15px;
    align-self: center;
  }
  .link-style-div-select {
    display: flex;
    .ant-select {
      margin: 0;
      padding: 0;
      color: rgba(0, 0, 0, 0.85);
      font-size: 14px;
      font-variant: tabular-nums;
      line-height: 1.5715;
      list-style: none;
      position: relative;
      display: inline-block;
      cursor: pointer;
    }
    .ant-select-selection-overflow-item {
      margin-right: 5px;
    }
    .ant-select-selector {
      background: #f5f5f5;
      border-radius: 4px;
      width: 100%;
    }
    span.ant-select-selection-item {
      text-align: center;
      border-color: #6382d9 !important;
      font-size: 15px !important;
      flex: initial;
      color: #515164 !important;
      font-family: Open Sans;
      font-style: normal;
      align-items: center;
      background: none;
      border-radius: 5px;
      padding: 7px 10px !important;
      margin: 3px 0;
      display: flex;
    }
    .ant-select-selection-item-remove {
      display: none;
    }
  }
  .panel-style {
    padding-bottom: 45px;
    .ant-collapse-content {
      margin-top: 32px;
    }
  }
  .imageStyle {
    height: 100%;
    width: 100%;
    object-fit: contain;
  }
  .box-image {
    margin-right: 31.5px;
  }
  .icon-image {
    position: absolute;
    top: 0px;
    left: 0px;
    z-index: 9;
  }
  .margin-icon {
    margin-left: 3px;
    margin-top: -1px;
  }
  .semi-circle {
    position: absolute;
    top: 0px;
    left: 0px;
    z-index: 9;
    width: 33px;
    height: 33px;
    background: #0098ff80;
    border-radius: 0 0 100px 0;
  }
  .check-img {
    border: 1px solid #0098ff;
    box-sizing: border-box;
    max-width: 231px;
    max-height: 244px;
    display: flex;
    justify-content: center;
    align-items: center;
    object-fit: cover;
    margin: 0 auto;
    position: relative;
    padding: 5px;
  }
  .image-default {
    border: 1px solid #bbbdbf;
    box-sizing: border-box;
    max-width: 231px;
    max-height: 244px;
    display: flex;
    justify-content: center;
    align-items: center;
    object-fit: cover;
    margin: 0 auto;
    padding: 5px;
    position: relative;
  }
  .labelStyle {
    font-family: Open Sans;
    font-style: normal;
    font-weight: normal;
    font-size: 12.5px;
    line-height: 17px;
    text-align: center;
    padding-top: 11px;
  }
  .collapseStyle {
    .ant-collapse-content-box {
      padding:0;
      div{
        &:nth-of-type(1){
          padding:0;
        }
      }
    }
    .ant-collapse-header {
      padding: 15px 0px 0px 0px;
    }
  }
  .precoCol2 {
    display: flex;
    justify-content: flex-start;
    flex-direction: row nowrap;
    margin-left: 16px;
  }
  .precoCol1 {
    display: flex;
    justify-content: flex-start;
    margin-right: 16px;
  }
  .embaCol1 {
    display: flex;
    justify-content: flex-start;
    margin-right: 16px;
  }
  .embaCol2 {
    display: flex;
    justify-content: flex-start;
    flex-direction: row nowrap;
    margin-left: 16px;
  }
  .infoCol1 {
    display: flex;
    flex-flow: row wrap;
    margin-right: 16px;
  }
  .infoCol2 {
    display: flex;
    flex-flow: column;
    margin-left: 16px;
  }
  .infoColPed1 {
    display: flex;
    flex-flow: row wrap;
    margin-right: 16px;
  }
  .infoColPed2 {
    display: flex;
    flex-flow: row wrap;
    margin-left: 16px;
  }
  .inserCol1 {
    display: flex;
    flex-flow: column;
    margin-right: 16px;
  }
  .inserCol2 {
    display: flex;
    flex-flow: row wrap;
    margin-left: 16px;
  }
  .timeline {
    position: relative;
    display: flex;
    width: 100%;
  }
  .btnHeader {
    font-family: Open Sans;
    font-style: normal;
    font-weight: bold;
    font-size: 14px;
    line-height: 19px;
    color: #346698;
  }

  .ant-input-disabled {
    color: #2a2a2c;
    background-color: #f5f5f5;
    border-color: #d9d9d9;
    box-shadow: none;
    cursor: default;
    opacity: 1;
  }

  .line {
    position: absolute;
    margin-top: 21px;
    width: 470px;
    margin-left: 300px;
    height: 3px;
    background-color: #ccc;
  }
  .ant-collapse.ant-collapse-borderless.ant-collapse-icon-position-right.site-layout-background.collapseStyle {
    margin: 100px 0;
  }
  .ant-steps.ant-steps-horizontal.ant-steps-default.ant-steps-label-horizontal {
    width: 600px;
    margin-left: 250px;
    padding: 10px;

    :after {
      width: 200px;
      height: 4px;
      background-color: gray;
    }
    .ant-steps-item-title {
      font-family: Open Sans;
      font-style: normal;
      font-weight: normal;
      font-size: 12.5px;
      line-height: 17px;
    }
    .ant-steps-item.ant-steps-item-process.ant-steps-item-active {
      .ant-steps-item-icon {
        background-color: white;
        border: solid 4px rgb(255, 134, 4);
        width: 24px;
        height: 24px;
        .ant-steps-icon {
          position: absolute;
          margin-top: 8px;
          margin-left: -4px;
          color: black;
        }
        .ant-steps-icon {
          display: none;
        }
      }
      .ant-steps-item-title {
        ::after {
          display: none;
        }
      }
      .ant-steps-item-title {
        margin-top: 35px;
        margin-left: -42px;
      }
    }
    .ant-steps-item.ant-steps-item-finish {
      .ant-steps-item-icon {
        background-color: #ccc;
        border: solid 2px #ccc;
        margin-left: 18px;
        width: 24px;
        height: 24px;

        .ant-steps-icon {
          display: none;
        }
      }
      .ant-steps-item-title {
        ::after {
          display: none;
        }
      }
      .ant-steps-item-title {
        position: absolute;
        left: 0;
        bottom: 0;
      }
    }
    .ant-steps-item.ant-steps-item-wait {
      margin-left: -15px;

      .ant-steps-item-icon {
        border: 3px solid #ccc;
        margin-left: 0px;
        margin-right: 50px;
        width: 24px;
        height: 24px;

        .ant-steps-icon {
          display: none;
        }
      }
      .ant-steps-item-title {
        position: absolute;
        left: 0;
        bottom: 0;
        color: #ccc;

        ::after {
          display: none;
        }
      }
    }
  }
  .ant-collapse.ant-collapse-borderless.ant-collapse-icon-position-right.site-layout-background {
    position: absolute;
    width: 100%;

    .ant-collapse-item.font-20 {
      border-bottom: none;
    }
    .ant-collapse-header {
      border-bottom: 1px dashed gray;
      width: 81vw;
      justify-content: space-between;
      flex-direction: row-reverse;
    }
    .ant-collapse-content,
    .ant-collapse-content-active {
      border-bottom: none;
      width: 80vw;
    }
    .anticon.anticon-right.ant-collapse-arrow {
      margin-top: 16px;
      color: rgb(68, 102, 171);
      position: inherit;
    }
  }

  .ant-descriptions {
    margin-top: 10px;

    .ant-descriptions-row {
      .ant-descriptions-item-label {
        font-weight: bold;
      }
      .ant-descriptions-item-content {
        border-radius: 4px;
        display: flex;
        justify-content: center;
        background-color: #f3f5f9;
        margin-right: 110px;
        padding: 10px;
      }
    }
  }
  .headerDetalhesProdutos {
    display: flex;
    flex-flow: row wrap;
    align-items: center;
    justify-content: flex-start;
    width: 100%;
    padding-right: 2.5em;
    padding-bottom: 30.5px;
  }
  .divHeaderDetalhesProdutos {
    display: flex;
    justify-content: space-between;
    width: 100%;
    padding-bottom: 32px;
  }
  .titleHeaderDetalhesProdutos {
    display: flex;
    align-items: center;
  }
  .botoesHeaderDetalhesProdutos {
    display: flex;
  }
  .h1HeaderDetalhesProdutos {
    font-family: Open Sans;
    font-style: normal;
    font-weight: 300;
    font-size: 20px;
    line-height: 27px;
    margin-right: 15px;
  }
  .alertStyleWarning {
    width: 100%;
    background: #e1b30f;
    border-radius: 4px;
    text-align: center;
  }
  .alertStyleSuccess {
    width: 100%;
    background: green;
    border-radius: 4px;
  }
  .textPreco {
    font-size: 13px;
    margin: 0 15px 15px 5px;
    text-align: center;
  }
  .textPreco2 {
    font-size: 13px;
    margin: 0 15px 20px 5px;
    text-align: center;
  }
  .textPreco1 {
    font-size: 13px;
    margin: 19px 15px 15px 5px;
    text-align: center;
  }
  .btn-preco{
    width: 100%;
  }
  .btn-preco1{
    margin: 0px 0 20px 0;
    width: 100%;
  }
  .date {
    margin: 0 0 0 5px;
  }
  .switch {
    margin: 0 0 20px 0;
    width: 100px;
    height: 30px;
    .ant-switch-handle{
      margin: 4px 0;
    }
  }
  .switch2 {
    margin: 17px 0 20px 10px;
    width: 100px;
    height: 30px;
    .ant-switch-handle{
      margin: 4px 0;
    }
  }
  .ant-alert-message {
    font-family: Open Sans;
    font-style: normal;
    font-weight: bold;
    font-size: 14px;
    line-height: 19px;
    color: #ffffff;
  }
  .anticon-close {
    color: #ffffff;
  }
  .alertHeaderDetalhesProdutos {
    width: 100%;
    display: flex;
  }
`;
export default Wrapper;
