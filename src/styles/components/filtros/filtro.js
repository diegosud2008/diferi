import styled from "styled-components";

const Wrapper = styled.div `
  
  .all {
    position: fixed;
    margin-top: 80px;
    margin-right: 32px;
    display: flex;
    align-items: center;
    flex-flow: row wrap;
    font-family: Open Sans;
    background: #fff;
    font-style: normal;
    min-height: 77px;
    width: 82%;
    z-index: 1;
    justify-content: space-between;
    transition: 1s;
    border-bottom: 1px solid #F5F5F5;
    
  }
  .ant-divider.ant-divider-vertical.divider-style{
    height: 32px;
    transition: 0.3s;
    background: #CCC
  }
  .ant-divider.ant-divider-vertical {
    background: #CCC;
    height: 32px;
    margin: 0 32px;
  }
  .campo-date {
        margin: 0 10px 0 0;
      }
      label {
        margin: 0 10px;
      }
      input {
        border: 1px solid #ccc;
        border-radius: 5px;
        padding: 5px;
        button {
        }
      }
  .itemsFilter{
    display: flex;
    align-items: center;
    flex-flow: row nowrap;
    justify-content: space-between;
    width: 96%;
  }
  .ant-tabs-tab:hover{
    color: black;
  }
  .iconsDiv{
    display: flex;
    align-items: center;
    margin: 0 0 0 48px;
  }
  .dividerStyle{
    display: inline-block;
    padding:0px;
    opacity: 0.3;
    border: 1px solid #BBBDBF;
  }

    .detailsFilter {

      .ant-tabs-nav-more {
        display: none;
      }
      .ant-tabs-ink-bar.ant-tabs-ink-bar-animated {
        background-color: #FF8604;
      }
    }

    .back-icon {
      svg {
        width: 24px !important;
        height: 24px;
        display: flex;
        color: rgb(68, 102, 171);
        cursor: pointer;
      }
    }
    .ant-tabs-tab.ant-tabs-tab-active {
      .ant-tabs-tab-btn {
        color: black;
      }
    }
    .ant-tabs-tab-btn:focus, .ant-tabs-tab-remove:focus, .ant-tabs-tab-btn:active, .ant-tabs-tab-remove:active {
    color: #000;
    }
    .ant-tabs-nav::before {
      border: none;
    }
    .list-icon-produtos {
      margin-left: -33px;
    }
    .filterIconPed{
      margin-right: 20px;
      display: flex;
      align-items: center;
      color:  #4466AB;
      width: 20px !important;
      height: 20px !important;
    }
    .filterIconProd{
      display: flex;
      align-items: center;
      color:  #4466AB;

      svg {
        width: 20px;
        height: 20px;
      }
    }
    .planeIcon{
      display: flex;
      align-items: center;
      color:  #4466AB;
      svg {
        width: 20px;
        height: 20px;
      }
    }
    .dotsVertical {
      display: flex;
      align-items: center;
      color:  #4466AB;
      transition: 0.3s;
      padding: 0px;
    }
    a.ant-dropdown-trigger.points-dropdown.undefined {
      svg {
        width: 20px;
        height: 24px;
      }
    }
    .filterIcon {
      cursor: pointer;
    }
    .mL-37{
      margin-left: 37px;
    }
    .change-icon{
      cursor: pointer;
    }
    .ant-input-prefix {
      color: rgb(68, 102, 171);
    }

    .ant-input-prefix{
      svg {
        width: 20px !important;
        height: 20px !important;
      }
    }
    .checked {
      display: flex;
      align-items: center;
      width: 0%;
      border: none;
      transition: 0.5s;
      margin-left: 33px;
    }
    .srcIcon {
      display: flex;
      align-items: center;
      width: 0%;
      border: none;
      transition: 0.5s;
      margin-left: 33px;
    }
    .srcIconPed {
      display: flex;
      align-items: center;
      width: 0%;
      border: none;
      transition: 0.5s;
    }
    .srcIconPubli {
      display: flex;
      align-items: center;
      width: 0%;
      border: none;
      transition: 0.5s;
      margin-left: 33px;
    }
    .ant-input {
      height: 5vh;
      font-size: 17px;
      font-family: 'Open Sans';
    }
    .div-arrow-down{
      display: flex;
      margin-left: 0px;
      width: 0px;
      transition: 0.5s;
    }
    .escIcon {
      svg {
        width: 14px;
        height: 14px;
      }
    }
    :hover .div-arrow-down {
      width: 14px;
      transition: 0.5s;
    }
    :hover .srcIcon {
      animation: slide 0.5s;
      width: 100%;
  
      @keyframes slide {
        from {
          width: 0%;
          opacity: 0;
        }
        to {
          width: 20%;
          opacity: 1;
        }
      }
    }
    :hover .srcIconPubli {
      animation: slide 0.5s;
      width: 100%;
  
      @keyframes slide {
        from {
          width: 0%;
        }
        to {
          width: 100%;
        }
      }
    }
    :hover .checked{
      animation: slide 0.5s;
      width: 33px;
  
      @keyframes slide {
        from {
          width: 0%;
        }
        to {
          width: 33px;
        }
      }
    }
    :hover .srcIconPed {
      animation: slide 0.5s;
      width: 100%;
  
      @keyframes slide {
        from {
          width: 0%;
        }
        to {
          width: 100%;
        }
      }
    }

  .ant-input-affix-wrapper input {
    border-bottom: 1px solid #EEE9E9;

  }
  .all:not(:hover) .escIcon{
    display none;
    } 
.all:not(:hover) .input-divider{
  display none;
  } 
    .input {
      display: flex;
      align-items: center;
      margin-top: -4px;
      box-shadow: 0 0 0 0;
      border: 0 none;
    }
    .ant-dropdown-trigger.points-dropdown {
      a {
        text-decoration: none;
        margin: 0;
      }
    }
    .text-filter {
      display: flex;
      white-space: nowrap;
      font-size: 14px;

      p {
        display: flex;
        align-items: center;
        margin: 0px 10px 0px 0;
        color: black;
      }

      hr {
        width: 1px;
        margin: 5px 0 0 0;
        opacity: 0.3;
        color: black;
      }

      a {
        color: rgb(68, 102, 171);
        display: flex;
      }
    }
    .none {
      display: none !important;
      transition: 1s;
      width: 0px;

      .div-arrow-down {
        margin: 0 0 0 8px;
      }
    }
    .block {
      margin-right: -30px;
      display: flex !important;
      width: 25vh;
      transition: 0s;
    }
    span.block {
      display: flex !important;
      margin-right: 46px;
      transition: 0s;
    }
    :hover .none {
      display: flex !important;
      animation: show_to 0.5s;
      transition: 0.5s;
      width: 160px;

      @keyframes show_to {
        from {
          width: 0;
        }
        to {
          width: 150px;
        }
      }
    }
    .none_110 {
      display: none;
      transition: 1s;
      width: 0px;
      margin-right: 20px;
    }
    :hover .none_110 {
      display: flex;
      animation: show_to_110 0.5s;
      transition: 0.5s;
      margin-right: 46px;
      @keyframes show_to_110 {
        from {
          width: 0;
        }
        to {
          width: 90;
        }
      }
    }

    .none_hr {
      display: none;
      transition: 1s;
      width: 0px;
    }
    :hover .none_hr {
      display: flex !important;
      width: 1px;
      transition: 1s;
      animation: show2 0.5s;

      @keyframes show2 {
        from {
          width: 0px;
        }
        to {
          width: 1px;
        }
      }
    }
    .none_icon {
      display: none;
      transition: 1s;
      width: 0px;
    }
    :hover .none_icon {
      display: flex !important;
      width: 24px;
      transition: 1s;
      animation: show_icon;

      @keyframes show_icon {
        from {
          width: 0px;
        }
        to {
          width: 24px;
        }
      }
    }
    svg.none_icon.show_icon {
      display: flex;
      width: 24px;
    }
}
`;

export default Wrapper;