import styled from 'styled-components';

const Wrapper = styled.div`

.ant-menu.ant-menu-root.ant-menu-vertical.ant-menu-light {
    border            : 1px solid gray;
    padding: 5px;
    margin: -20px 0 0 0;
    width: 120%;
}
.ant-menu-item.ant-menu-item-selected.ant-menu-item-only-child {
    background-color: #F3F5F9;
}
:hover .ant-menu-item.ant-menu-item-active.ant-menu-item-only-child{
    background-color: #F3F5F9;
}
.ant-menu-item.ant-menu-item-only-child {
    color: #000;
}

.ant-menu-item.ant-menu-item-selected.ant-menu-item-only-child {

}

`;

export default Wrapper;