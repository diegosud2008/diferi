import styled from 'styled-components';

const Wrapper = styled.div`
.ant-menu.ant-menu-root.ant-menu-vertical.ant-menu-light {
    border            : 1px solid gray;
    padding           : 10px;
    box-shadow        : 1px 1px 3px gray;
    text-decoration   : none;
    width             : 200px;

    .ant-menu-item.ant-menu-item-only-child:hover {
        color   : black;
    }

    h2 {
        color         : gray;
        margin-left   : 36px;
    }

    .ant-checkbox-inner{
        border   : 1px solid rgb(68, 102, 171);
    }
    .ant-menu-title-content{
        display       : flex;
        align-items   : center;
    }
    .icon {
        display: flex;
        position   : absolute;
        right      : 0;
        width: 24px;
    }
    .ant-menu-item{
        color   : black;
    }
}
`;

export default Wrapper;