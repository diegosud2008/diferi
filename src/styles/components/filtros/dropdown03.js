import styled from 'styled-components';

const Wrapper = styled.div`
.ant-menu.ant-menu-root.ant-menu-vertical.ant-menu-light {
    border       : 1px solid gray;
    padding-left: 0px;
    box-shadow   : 1px 1px 3px gray;
    width: 201px;
    max-width: 201px;
    min-width:201px;
    
    .li-vermais{
        display: flex;
        align-items: center;
        padding:0px;
        margin: 0px 8px;
    }

    .ant-menu-item.ant-menu-item-only-child:hover {
        color   : black;
    }

    .ant-checkbox-inner{
        border   : 2px solid rgb(68, 102, 171);
    }
    .ant-menu-item:active, .ant-menu-item-selected{
        background: #fff;
    }
    .ant-menu-title-content{
        display: flex;
        align-items : center;
        position     : absolute;
        width: 100%;
        padding: 5px;
        .check-style{
            margin-right: 7px;
        }
        .icon{
            display: flex;
            margin-left: 7px;
            margin-right: 5px;
            position: absolute;
            right: 0;
            width: 24px;
        }
        .spam-style{
            margin-left:5px
        }
    }
    .ant-menu-item{
        color   : black;
        margin: 0px 8px;
        padding: 0px
    }
}
`;

export default Wrapper;