import styled from "styled-components";

const Wrapper = styled.div`
  .container {
    width: 100vw;
    height: 100%;
    .ant-table-tbody > tr.ant-table-row:hover > td {
      background-color: none;
    }
    svg {
      color: rgb(68, 102, 171);
      cursor: pointer;
      width: 20px !important;
    }
    .ant-table-content {
    margin: 162px 0;
    }
    .header {
      display: flex;
      align-items: center;
      margin: 20px 0;

      .campo-date {
        margin: 0 10px 0 0;
      }
      input.campo-date {
        border: 1px solid #ccc;
        border-radius: 5px;
        padding: 5px;
        button {
        }
      }
      label {
        font-weight: bold;
        margin: 10px;
      }
      i {
        margin: 10px 10px 0 0;
        cursor: pointer;
      }
    }
    .filterIconPed {
      margin:0 10px;
    }
    .pesquisa-avancada {
      margin: 20px 10px;
      border: 1px solid #ccc;
      width: 80%;
      height: auto;
      border-radius: 5px;

      .conteudo-pesquisa {
        padding: 10px;
        border-top: 1px solid #ccc;

        select {
          margin: 5px;
          padding: 5px;
          font-weight: bold;
          cursor: pointer;
        }
      }
      .label {
        background-color: rgb(245, 245, 245);
        padding: 10px;
        font-weight: bold;
      }
    }
    table.conteudo {
      margin: 20px 10px;
      width: 80%;

      tr,
      td,
      th {
        padding: 10px;
      }
      td,
      th {
        text-align: start;
      }
      th {
        color: rgb(68, 102, 171);
      }
      svg:active {
        color: rgb(68, 102, 272);
        transform: rotate(10deg);
      }
      #color {
        background-color: rgb(245, 245, 245);
      }
      #status-pedido {
        border-radius: 5px;
        padding: 10px 33px;
        background-color: rgb(0, 180, 224);
        color: #fff;
      }
      #status-entrega {
        border-radius: 5px;
        padding: 10px 33px;
        background-color: rgb(255, 86, 89);
        color: #fff;
      }
    }
  }
`;

export default Wrapper;
