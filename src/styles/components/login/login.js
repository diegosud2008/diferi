import styled from 'styled-components';
import BackGround from '../../../assets/background.png'

console.log(BackGround);

const Wrapper = styled.div`
.Login {
height           : 100vh;
width            : 100vw;
background-image : url(${BackGround.src});
background-size  : cover;
background-repeat: no-repeat;
display          : flex;
justify-content  : space-between;
    
    .main {
        margin: 82px auto 579.42px 115px;
        width : 10vw;

        h1 {
        width      : 211.33px;
        height     : 106.58px;
        font-family: 'Inter', sans-serif;
        font-style : italic;
        font-weight: bold;
        font-size  : 59.4251px;
        line-height: 119px;
        color      : #FFFFFF;
        text-shadow: 0px 0px 10px rgba(0, 0, 0, 0.8);
        }
        p {
            font-size  : 18px;
            color      : #FFF;
            font-family: Open Sans;
            margin     : -40px 0 0 0;
            width      : 325px;
            height     : 22.13px;
            text-shadow: 0px 0px 6px rgba(0, 0, 0, 0.75);
        }
    }
    .formulario {
        width     : 540px;
        height    : 617px;
        margin    : 75px 106px 76px auto;
        background: #FFF;


        h3 {
            width      : 187px;
            height     : 27px;
            font-family: Open Sans;
            font-style : normal;
            font-weight: bold;
            font-size  : 20px;
            line-height: 27px;
            display    : flex;
            margin     : 90px auto;
            color      : #001338;

        }
        small
        {
            width      : 51px;
            height     : 18px;
            font-family: Open Sans;
            font-style : normal;
            font-weight: normal;
            white-space: nowrap;
            font-size  : 14px;
            line-height: 18px;
            margin     : 0px 26%;
            display    : flex;
            align-items: center;

            color: #2A2A2C;
        }
        .recuperar-senha {
            color      : #0098FF;
            line-height: 19px;
        }
        .box {
            width        : 320px;
            height       : 42px;
            background   : #F3F5F9;
            border-radius: 4px;
            border       : none;
            font-size    : 14px;

            /* Inside Auto Layout */
            margin : 0 auto;
            display: block;
            margin : 0 auto;
            padding: 10px 25px;
            width  : 50%;
        }
        .box:focus {
            outline: none !important;
            border:1px solid #0098FF;
        }
        #password {
            font-size: 20px;
        }
        .button {
            width        : 275px;
            height       : 42px;
            background   : #0098FF;
            border-radius: 4px;
            margin       : 60px 25%;
            color        : #FFF;
            border       : none;
            cursor       : pointer;
        } 
        .button:active {
            opacity: 0.8;
        }
        button:disabled{
            background-color: #CCC;
        }
        .spinner {
            width: 30px;
            height: 30px;
            margin: -45px auto;
            border-radius: 50%;
            border: 5px solid #FFF;
            border-left: 5px solid #0098FF;
            animation: rotacao 1s linear infinite;
            
            @keyframes rotacao {
                from{transform: rotate(0deg); transition: 1s;}
                to{transform: rotate(360deg); transition:1s;}
            }
        } 
    }
    img {
        width   : 250px;
        position: absolute;
        bottom  : 5%;
        left    : 7%;
    }
}
`

export default Wrapper
