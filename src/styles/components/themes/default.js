import styled from "styled-components";

const Wrapper = styled.div `
  font-family: "Open sans";
  font-style: normal;
  user-select: none;
  height: 100%;
  overflow: hidden;
  
  svg {
  width: 24px;
  cursor: pointer;
}
  .menu {
    position: fixed;
    font-size: 14px;
    width: 236px;
    margin: 80px auto;
    padding: 20px;

    .ant-menu-item.ant-menu-item-only-child {
      width: 100%;
      height: 40px;

      a {
        font-size: 14px;
      }
    }
    p {
      font-family: "Open Sans", sans-serif;
      color: #2a2a2c;
      font-size: 20px;
      white-space: nowrap;
      font-weight: 200;
      font-style: normal;
      margin: 18px 0px 36px 0px;
      text-align: center;
    }
    hr {
      opacity: 0.3;
      border: 1px solid #bbbdbf;
    }
    .ant-menu-item-icon {
      display: none;
    }
    .ant-menu-item {
      a {
        font-size: 15px;
        margin-top: -20px;
        margin-left: -10px;
        position: absolute;
      }
    }
    .ant-menu-item:hover {
      color: black;
    }
    .ant-menu-item-selected:hover {
      color: #fff !important;
    }
    .ant-menu-item-selected {
      :after {
        display: none;
      }

      a {
        color: white;
        font-weight: bold;
      }


      background: #ff8604 !important;
      color: #fff;
      border-radius: 5px;
      transition:0.2s;
      &:hover{
        background: #e97900 !important;
        transition:0.2s;
      }
    }
    
  }
  
  h2.identidade {
    position: fixed;
    bottom: 40px;
    left: 62px;
    font-style: italic;
    font-weight: bold;
    font-size: 42px;
    color: #001338;
    cursor: pointer;
  }
  .ant-drawer-content {
    background: #f3f5f9;

    .ant-collapse.ant-collapse-borderless.ant-collapse-icon-position-left {
      background: #f3f5f9;
    }
  }
  .ant-drawer.ant-drawer-right.ant-drawer-open {
    .ant-drawer-mask {
      opacity: 0;
      animation: 0;
      box-shadow: none;
    }
  }

  .close {
    position: absolute;
    right: 20%;
    top: 1%;
    cursor: pointer;
  }

  .menu-right {
    color: rgb(68, 102, 171);
    div {
      display: flex;
      padding: 7px;

      small {
        font-size: 16px;
        color: black;
        padding: 0px 10px 0px 10px;
      }
    }
    .chat-icon {
      svg {
        cursor: pointer;
        width: 18px;
        height: 16px;
      }
    }
    .notification-icon {
      svg {
        cursor: pointer;
        width: 16px;
        height: 18px;
      }
    }
    .user-icon {
      svg {
        cursor: pointer;
        width: 16px;
        height: 18px;
      }
    }

    .vertical-2 {
      border-left: 1px solid #bbbdbf; /* Adiciona borda esquerda na div como ser fosse uma linha.*/
      height: 28px;
    }
  }

  .ant-layout-sider.ant-layout-sider-light {
    margin-right: 16px;
  }
  .ant-layout {
    background: #fff !important;
  }

  b {
    display: flex;
    align-items: center;
    padding-left: 22px;
    margin-right: 11px;
  }

  .ant-row.custom-header.align-items-center.d-flex {
    margin-top: 20px;
    margin-left: -13px;
  }

  .ant-dropdown-trigger.d-flex {
    cursor: pointer;

    svg {
      margin: auto 0;
      width: 10px;
      height: 5px;
    }
  }
  .site-layout-background {
    height: 100%;
  }

  .anticon.anticon-menu-fold.trigger {
    display: none;
  }
  .ant-layout-header{
    padding:0 32px;
  }
  .ant-layout-header.site-layout-background {
    border: 1px solid #bbbdbf;
    position: fixed;
    z-index: 1;
    width: 100%;
    height: 80px;
  }

  .ant-layout-content {
    margin-left: 16px;
  }
  .ant-dropdown-trigger.points-dropdown.ml-2.dotsDetails {
    .dotsVertical {
      position: absolute;
      margin-left: 40px;
      margin-top: -21px;
    }
  }
`;

export default Wrapper;