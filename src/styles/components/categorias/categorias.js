import styled from 'styled-components';

const Wrapper = styled.div `
transition: 0.5s;
display   : flex;
height    : 100%;

.site-drawer-render-in-current-wrapper {
    position     : relative;
    width: 30%;
    text-align   : center;
    border-top   : 1px solid #ebedf0;
    border-radius: 2px;

}

.d-none {
    display: none;
}

.d-block {
    display   : block !important;
    margin-top: 80px;
    height    : calc(100vh - 80px);
}

.tamanho_70 {
    width     : 70% !important;
    margin-right:16px;
    transition: 0.2s;
}

.tamanho_100 {
    width     : 100% !important;
    transition: 0.2s;

}

[class^="tree__Wrapper"] {
    margin: 160px 0;

    .ant-tree-node-selected {
        background: rgb(243, 245, 249);
    }
}
.iconsDiv {
  margin: 0;
    div {
        .list-icon {
            margin: 0;
        }
        &:nth-of-type(1){
          margin: 0 0 0 32px;
        }
        &:nth-of-type(2){
          margin: 0 0 0 32px;
          svg{
            path{
              stroke:#4466AB;
            }
          }
        }
    }
    .checkBox {
        svg {
            width: 16px;
        }
    }
    .srcIconPed{
      
    margin: 0 32px 0 22px;

    }
    a{
      div{
        margin:0;
      }
    }
}
`;

export default Wrapper;