import styled from 'styled-components';

const Wrapper = styled.div`
margin: 80px auto;
.icon-motivo {
    svg {
        color: rgb(68, 102, 171);
    }
}
`;

export default Wrapper;