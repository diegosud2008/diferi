import styled from 'styled-components';

const Wrapper = styled.div`
 transition: 0.5s;
  height: 100%;

  .ant-modal{
    .bodyStyle {
      border: 4px solid;
    }
  }
  .table-default {
  }
  .ant-table-row{
    height: 60px !important;
    margin: 0 200px;
  }
  .trash-none {
    display: none;
    transition: 1s;
  }
  .icons {
    padding:5px;
    display: none;
    justify-content: flex-start;
    cursor: pointer;
    transform: 0.5s;
    width: 43px;
  }
  tr.active {
    background: #F3F5F9;
  }
  .ant-table-content {
    margin: 162px 0;
  }
  .ant-popover-inner-content {
    background: red;
  }
.spinner {
    margin:0 auto;
    border: 8px solid #CCC;
    border-left: 8px solid rgb(68, 102, 171);
    border-radius: 50%;
    width: 60px;
    height: 60px;
    z-index: 1;
    animation: rotacao 0.8s linear infinite;
            
    @keyframes rotacao {
        from{transform: rotate(0deg); transition: 1s;}
        to{transform: rotate(360deg); transition:1s;}
    }
}
.none-spinner {
  display: none;
}
  .align{
    transition: 0.5s;
    display: flex;
    justify-content: center;    
  }
  /* tr.icons-view:hover .icons  {
      display: flex;
      margin-left: 15px;
      animation: icons 0.5s;

    @keyframes icons {
          from { width: 0; }
          to   {width: 43px}
      }
  } */
  
  .td-bolder{
    font-weight: bolder;
  }
  .site-drawer-render-in-current-wrapper {
    position: relative;
    width: 30%;
    text-align: center;
    border-top: 1px solid #ebedf0;
    border-radius: 2px;

  }
  .trashIcon svg{
      margin-right: 20px;
      opacity: 0;
      width: 20px !important;
      height: 24px !important;
  }
  .plusIcon{
      margin-bottom: 11px;
      margin-right: 37px;
      width: 16px !important;
      height: 16px !important;
  }
  .d-none {
    display: none;
  }
  .d-block {
    display: block !important;
  }
  .tamanho_70 {
    width: 70%;
    transition: 0.2s;
    .all{
      width: 58%;
    }
  }
  .tamanho_100 {
    width: 100%;
    transition: 0.2s;
    margin-right: 32px !important;
  }
  .ant-popover.ant-popconfirm.ant-popover-placement-top:hover {
    .trashIcon {
      opacity: 1;
    }
  }
  #delete_class {
    display: none;
  }
  .editIcon {
    svg {
        color: rgb(68, 102, 171);
        cursor: pointer;
        opacity: 0;
    }
    }
    .ant-table-row.ant-table-row-level-0 {
      :hover .editIcon svg{
            animation: slide 0.2s;
            width: 24px !important;
            opacity: 1;

            @keyframes slide {
                from {margin-left: 15px; opacity: 0; }
                to {margin-right:0px; opacity:1;}
            }
    }
    :hover .trashIcon svg{
        opacity: 1;
        animation: slide 0.5s;
        
        @keyframes slide {
            from{opacity:0;}
            to{opacity:1;}
        }
    }
}
`;

export default Wrapper;