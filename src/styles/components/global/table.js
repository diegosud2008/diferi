import styled from "styled-components";

const Wrapper = styled.div`
.ant-table-row{
  min-height: 60px !important;
}
 .ant-table-cell{
  padding:0px 18px 0 0;
}
`;

export default Wrapper;
