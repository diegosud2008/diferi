﻿import styled from "styled-components";

const Styles = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: ${(props) => props.width};
  div:first-child {
    margin-right: ${(props) => props.marginR};
    margin-left: ${(props) => props.marginL};
  }
  form:first-child {
    width: 100%;
    margin-right: ${(props) => props.marginR};
    margin-left: ${(props) => props.marginL};
  }
  .contenct {
    display: contents;
  }
  .checkDiv {
    display: flex;
    justify-content: flex-start;
    align-items: center;
    width: 100%;
    background: #f3f5f9;
    border-radius: 4px;
    margin-top: 35px;
    min-height: 40px;
    padding: 10px;

    spam {
      align-items: center;
      margin-left: 12px;
      margin-right: 13.34px;
      font-family: Open Sans;
      font-style: normal;
      font-weight: normal;
      font-size: 14px;
      line-height: 19px;
    }
  }
  .input-style {
    min-height: 40px;
    width: 100%;
    background: #f3f5f9;
    border-radius: 4px;
    border: none;
  }
  .input-style-italic {
    font-style: italic;
    min-height: 40px;
    width: 100%;
    background: #f3f5f9;
    border-radius: 4px;
    border: none;
  }
  .label-form {
    width: 100% !important;
    display: flex;
    justify-content: space-between;
  }
  .ant-form label {
    width: 100%;
  }
  .w-100 {
    display: flex;
    width: 100%;
    min-height: 40px;
    .ant-input-disabled {
      background: #f3f5f9 !important;
      border-radius: 4px;
      border: none;
    }
    .ant-form-vertical .ant-form-item {
      width: 100%;
    }
  }

  .inputWrap {
    display: flex;
    min-height: 40px;
    .ant-form-vertical {
      width: 100%;
    }
  }
  .text-area-style {
    background: #f3f5f9;
    border-radius: 4px;
    width: 100%;
  }
  .text-area-div {
    width: 100%;
  }
  .select {
    .ant-select {
      margin: 0;
      padding: 0;
      color: rgba(0, 0, 0, 0.85);
      font-size: 14px;
      font-variant: tabular-nums;
      line-height: 1.5715;
      list-style: none;
      position: relative;
      display: inline-block;
      cursor: pointer;
      background: #f3f5f9 !important;
      border-radius: 4px !important;
      border: none !important;
    }
    .ant-select-selection-overflow-item {
      margin-right: 5px;
    }
    .ant-select-selector {
      background: #f3f5f9 !important;
      border-radius: 4px !important;
      border: none !important;
      width: 100%;
    }
    span.ant-select-selection-item {
      text-align: center;
      font-size: 15px !important;
      flex: initial;
      color: #515164 !important;
      font-family: Open Sans;
      font-style: normal;
      align-items: center;
      background: none;
      padding: 7px 10px !important;
      margin: 3px 0;
      display: flex;
      border: none;
    }
    .ant-select-selection-item-remove {
      display: none;
    }
    .ant-select-selector::after {
      display: inline-block;
      width: 18px;
      margin: 2px 0;
      line-height: 24px;
      align-content: flex-start;
      position: absolute;
      content: "...";
      right: 5px;
      top: 0px;
      font-weight: 700;
      font-size: 15px;
      color: #4466ab;
    }
  }
  .multiple {
    padding: 2px;
    .ant-select-selector::after {
      display: inline-block;
      width: 18px;
      margin: 2px 0;
      line-height: 24px;
      align-content: flex-start;
      position: absolute;
      content: "...";
      right: 5px;
      top: 0px;
      font-weight: 700;
      font-size: 15px;
      color: #4466ab;
    }
    .ant-select-selection-overflow-item {
      margin-right: 5px;
    }
    span.ant-select-selection-item {
      text-align: center;
      border-color: #6382d9 !important;
      border: 1px dashed #6382d9;
      font-size: 15px !important;
      flex: initial;
      line-height: 19px;
      color: #515164 !important;
      font-family: Open Sans;
      font-style: normal;
      align-items: center;
      background: none;
      border-radius: 5px;
      padding: 7px 10px !important;
      margin: 3px 0;
      display: flex;
    }
    .ant-select-selection-item-remove {
      display: none;
    }
  }
`;

export default Styles;
