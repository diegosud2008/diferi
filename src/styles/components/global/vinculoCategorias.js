import styled from "styled-components";

const Wrapper = styled.div`
margin-top:102px;
  margin-right: 32px;
  .main-div {
    max-width: 100%;
    margin: 22px 33px;
    .header-vinculo {
      font-family: Open Sans;
      font-style: normal;
      font-weight: 300;
      font-size: 20px;
      line-height: 27px;
      display: flex;
      align-items: center;
      strong {
        font-weight: bold;
      }
    }
    .category-wrapper{
      display: flex;
      justify-content: space-between;
      .tree-wrapper{
        width:70%;
        .all{
          margin: 0;
          min-height: 80px;
        }
      }
      [class^="tree__Wrapper"]{
        width:100%;
        margin-top: 80px;
        .ant-tree-checkbox{
          display:none;
        }
        .ant-tree-treenode{
          border:none;
        }
        .last-child-tree{
          svg{
            display:none;
          }
        }
        .parent-div{
          span{
            &:nth-of-type(2){
              display:none;
            }
            
          }
        }
        
      }
    }
    .body-vinculo {
      margin: 84px 0 0 0;
      .div-mkt-list {
        display: flex;
        flex-direction: column;
        [class^="item-mkt-list-"] {
          filter: grayscale(1);
          cursor: pointer;
          display: flex;
          flex-flow: row nowrap;
          justify-content: flex-start;
          align-items: flex-start;
          margin-bottom: 13px;
          font-family: Open Sans;
          font-style: normal;
          font-weight: normal;
          font-size: 14px;
          line-height: 19px;
          .title-mkt-list {
            margin-left: 9.52px;
          }
          
        }
        .active{
          filter: grayscale(0);
        }
      }
    }
  }
`;

export default Wrapper;
