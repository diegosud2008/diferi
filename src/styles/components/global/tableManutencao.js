import styled from "styled-components";

const Wrapper = styled.div`
.ant-table-row{ 
  min-height: 60px !important;
}
 .ant-table-cell{
  padding:10px 18px;
}
th.ant-table-cell{
    font-weight:bold;
}
table{
    width:80vw;
    margin: 20px 10px;
}
td.ant-table-cell.status_pedido {
    background-color: rgb(0, 180, 224);
    color:#FFF;
}
td.ant-table-cell.status_entrega {
    background-color: rgb(255, 86, 89);
    color:#FFF;
}
`;

export default Wrapper;
