import styled from "styled-components";

const Wrapper = styled.div`
.titleProdutos{
  text-indent: initial;
}
.div-text{
width: 100%;
margin-bottom: 24px;
p{ 
  width:70%;
}
}
.div-header{
  width: 100%;
  display: flex;
  flex-flow: row wrap;
  .div-img{
    width: 260px;
    height: 206px;
    display: flex;
    margin: 0 0 65px 0;
  }
}
.title {
  text-align: initial;
  align-items: flex-start;
  display: flex;
  text-indent: 0.5em;
  font-size: 14px;
  width:100%;
  strong {
      font-weight: 700;
      margin-right: 8px;
  }
  svg{
    margin-right: 8px;
  }

}
.styleImage{
  max-width: 260px;
  max-height: 260px;
}

`;
const WrapperDrawer = styled.div`

.ant-collapse-item{
  svg{
    width: 11.8px;
      height: 11.8px;
  }
}
.ant-drawer-content-wrapper{
box-shadow: none !important;
}
.cSDrAV .div-header .div-img .div-int-img {
padding: 6px 36px;
background: #fff;
height: 132%;
}
.ant-drawer-body{
width:100% !important;
padding: 0 24px;
}
.ant-drawer-header{
  padding: 24px 24px;
}
.ant-drawer-header-title{
width: 100%
}
.ant-collapse-arrow{
right: 0px !important;
}
.ant-drawer-close {
  padding: 0px;
  margin: 0px;
  order: 1;
  top: 12px;
  right: 12px;
  position: absolute;
  svg {
      color:#4466AB;
      Height: 16.19px;
      width: 8px;
  }
}
.ant-collapse {
  text-align: initial;
  border: 0;
  background: #f3f5f9;
}
.ant-collapse-header {
  border-bottom: 1px dashed #cecece;
  position:relative;
  padding-left:0px !important;
  padding-right: 24px !important;
  align-items: center !important;

  svg {
      color: #4466AB;
  }

  .ant-collapse-extra {
      align-items: center;
      display: flex;

      svg {
          color: #2A2A2C
      }
  }
}
.ant-collapse > .ant-collapse-item {
  border: 0;
}
.ant-collapse-content-box {
  background: #f3f5f9;
  padding:24px 16px;
  div{
    &:nth-of-type(1){
      padding:0;
    }
  }
}
.spamLabel{
display: flex;
max-width:35%;
align-itens: center;
justify-content: flex-start;
margin: 0 10px 0 0;
}
.spamValue{
display: flex;
max-width:65%;
width:60%;
align-itens: center;
justify-content: space-between;
}
.spamBold{
  font-weight: bolder;
}
.spamCancelado{
  font-weight: bolder;
  color:#F04A4A; 
}
.spamLink{
  color: #1753CC;
  width: 90%;
}
.spamProdutos{
  width: 10%;
}
.spamPausado{
color:#E1B30F;
}
.labelImg{
margin-left:10px;
}
.inactive{
color:#BBBDBF;
}
.text-align-center{
text-align:center;
}
.cursor-pointer{
cursor:pointer;
}
`;

module.exports = {
  Styles: Wrapper,
  StylesDrawer: WrapperDrawer
}
