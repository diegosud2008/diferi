import styled from "styled-components";

const Wrapper = styled.div`
.parent-div{
  display:flex;
  width: 100%;
  justify-content:space-between;
  span{
    &:nth-of-type(2){
      margin-right:300px;
      font-weight:normal;
    }

  }
}
.last-child-tree{
  display: flex;
    justify-content: space-between;
    align-items: center;
    div{
      font-weight:normal;
      display: flex;
    align-items: center;
    justify-content: center;
      padding:0;
      &:nth-of-type(2){
        svg{
          margin: 0 8px 0 0;
        }
      }
    }
}
  .ant-tree-treenode {
    display: flex;
    align-items: center;
    padding: 0 0 4px 0;
    outline: none;
    width: 100%;
    min-height: 40px;
    border-bottom: 1px solid #bbbdbf;
  }
  .ant-tree-switcher_open {
    transform: rotate(90deg);
  }
  .ant-tree-node-content-wrapper {
    width:100%;
    align-self: center;
    order: 2;
    display:flex;
    .ant-tree-title {
      width:100%;
    }
  }
  .ant-tree-node-content-wrapper:hover {
    background-color: none;
  }
  .ant-tree-node-selected {
    background-color: none;
  }
  .ant-tree-list-holder-inner {
    /* margin-right: 32px; */
  }
  .ant-tree-switcher {
    position: relative;
    flex: none;
    align-self: center;
    width: 24px;
    margin: 0;
    line-height: 24px;
    text-align: center;
    cursor: pointer;
    user-select: none;
    order: 2;
    display: flex;
  }
  .ant-tree-checkbox {
    order: 1;
    border: 2px solid #4466ab;
    border-radius: 2.25px;
  }
  .ant-tree-checkbox-inner:hover {
    border-color: none;
  }
  .ant-tree-list {
    font-weight:normal;
  }
`;

export default Wrapper;
