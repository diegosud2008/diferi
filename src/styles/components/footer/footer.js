import styled from 'styled-components';

const Wrapper = styled.div`
.footer {
    display: flex;
    position: fixed;
    bottom: 0;
    background-color: #FF8604;
    height: 10px;
    width: 100%;
    z-index: 9999;
}
`

export default Wrapper
