import styled from 'styled-components';

const Wrapper = styled.div`
width: 100%;
transition: 0.5s;
overflow: scroll;

:hover {
    background-color: #F3F5F9;
}
.icons {
    padding:5px;
    display: none;
    justify-content: flex-start;
    cursor: pointer;
    transform: 0.5s;
    margin-left: -15px;
    margin-right: -28px;
}
:hover .icons  {
    display: flex;
    animation: icons 0.5s;

    @keyframes icons {
        from { width: 0; }
        to   {width: 80px}
    }
}

.ant-list-item {
    padding:20px;
    border-top: 1px solid #E6E6FA ;
    background-color: white;
    margin-left: -52px;
    transition: 0.5s;

    :hover {
        background: #F3F5F9;
        transition: 0.5s;
    }
}
.d-flex.justify-content-between.w-15 {
    position: absolute;
    right: 180px;

    .d-flex.align-items-center {
        margin-right: 30px;
    }
}

.ant-list-item-meta-title {
    display: flex;
    justify-content: flex-start;
}
.dot.mv-auto {
    height: 16px;
    width: 10px;
    border-radius: 5px;
    box-shadow: 1px 1px 1px #CCC;
}
.list__Wrapper-sc-rnse9p-0.AwkQH{
    background-color: black;
    margin-left:  30px;
}
.colunsProducts{
    white-space: nowrap;
    margin-right: 40px ;

    .d-flex.justify-content-between.w-15 {

    .d-flex.align-items-center {
        margin-right: 30px;
    }
}

}
`

export default Wrapper;