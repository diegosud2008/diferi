import styled from "styled-components";

const Wrapper = styled.div`
transition: 0.5s;
display: flex;
height: 100%;

.ant-table-content {
  margin: 160px 0;
}
.ant-table-row{
  height: 60px !important;
}
.icons {
  padding:5px;
  display: none;
  justify-content: flex-start;
  cursor: pointer;
  transform: 0.5s;
  width: 43px;
}
.ant-divider.ant-divider-vertical.divider-text{
  margin: 0 20px;
}
tr.active {
  background: #F3F5F9;
}
.align{
  transition: 0.5s;
  display: flex;
  justify-content: center;    
}
// tr.icons-view:hover .align {
//   width: 63px;
//   animation: preco 0.5s;
//   @keyframes preco {
//     from { width: 43px; }
//     to   {width: 63px}
//   }
// }
tr.icons-view:hover .icons  {
    display: flex;
    margin-left: 15px;
    animation: icons 0.5s;

  @keyframes icons {
        from { width: 0; }
        to   {width: 43px}
    }
}

.td-bolder{
  font-weight: bolder;
}
.site-drawer-render-in-current-wrapper {
  position: relative;
  width: 30%;
  text-align: center;
  border-top: 1px solid #ebedf0;
  border-radius: 2px;

}
.d-none {
  display: none;
}
.d-block {
  display: block !important;
  margin-top: 80px;
  height: calc(100vh - 80px);
}
.tamanho_70 {
  width: 70%;
  /* margin-right:16px; */
  transition: 0.2s;
  .all{
    width: 57%;
  }
}
.tamanho_100 {
  width: 100%;
  margin-right:32px;
  transition: 0.2s;
  margin-right: 32px !important;
}
`;
export default Wrapper;
