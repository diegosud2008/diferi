import styled from 'styled-components';


const Styles = styled.div `
.verticalHeaderTable {

    table{
        width: 241px;
    tr {
        border-bottom: 1px solid #BBBDBF;
        display: flex;
    justify-content: space-between;
    align-items: center;
    height: 40px;
}

        td {
            font-size  : 16px;
            line-height: 22px;
            color      : #2A2A2C;

            &:nth-of-type(2) {
                text-align:right;
                font-weight: bold;
            }
        }
    }
    }
}`

export default Styles