import styled from 'styled-components';

const Styles = styled.div `
.cardsChartDashboard {
    display        : flex;
    justify-content: space-around;
    width          : 100%;
    margin-top     : 30px;

    .cardConector {
        width        : 241px;
        height       : 141px;
        border-radius: 4px;


        h3 {
            font-family: Open Sans;
            font-style : normal;
            font-weight: 600;
            font-size  : 16px;
            line-height: 22px;
            color      : #FFFFFF;
            padding    : 18px;

        }

        div {
            display        : flex;
            justify-content: end;
            padding        : 0 18px 0 0;

            span {
                font-family: Open Sans;
                font-style : normal;
                font-weight: 600;
                font-size  : 45px;
                line-height: 61px;
                display    : flex;
                align-items: baseline;
                text-align : right;
                color      : #FFFFFF;
                text-shadow: 0px 0px 9px rgba(0, 0, 0, 0.25);
            }
        }
    }
}
`

export default Styles