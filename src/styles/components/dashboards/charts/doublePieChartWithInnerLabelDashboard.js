import styled from 'styled-components';

const Styles = styled.div`
    .doublePieChartWithInnerLabelDashboard {
        display        : flex;
        justify-content: space-evenly;
        margin         : 50px 0 50px 0;

        div {
            text-align: center;
            cursor:pointer;

            h3 {
                font-family: Open Sans;
                font-style : normal;
                font-weight: 300;
                font-size  : 20px;
                line-height: 27px;

                color: #000000;
            }
        }
        svg{
            width:unset;
        }
    }
`

export default Styles