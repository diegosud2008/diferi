import styled from 'styled-components';

const Styles = styled.div `
.lineChartDashboard {
    cursor:pointer;
    h3 {
        font-family: Open Sans;
        font-style : normal;
        font-weight: 300;
        font-size  : 20px;
        line-height: 27px;
        display    : flex;
        align-items: center;
        margin: 0 10px 0 30px;
        color: #000000;

    }
    svg{
        width: unset;
    }
}
`

export default Styles