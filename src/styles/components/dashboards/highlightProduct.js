import styled from 'styled-components';


const Styles = styled.div `
.highlightProduct {
    width: 241px;
    height: 244px;
    display: flex;
    border: 1px solid #C4C4C4;
box-sizing: border-box;
flex-direction: column;
align-items: center;
text-align: center;
img{
    margin: 20px 0 0 0;
    width: 45%;
}
span{
    line-height: 17px;
    font-size: 12.5px;
    &:nth-of-type(1){
        font-size: 17px;
        line-height: 23px;
        margin: 5px 0 25px 0;

    }
    &:nth-of-type(1), &:nth-of-type(3){
        font-weight:bold;
    }
}

}`

export default Styles