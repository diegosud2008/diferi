import React, { useState, useEffect } from "react";
import Link from "next/link";
import FiltroCss from "../../styles/components/filtros/filtro";
import DropCss02 from "../../styles/components/filtros/dropdown02";
import DropCss03 from "../../styles/components/filtros/dropdown03";
import { Menu, Dropdown, Input, Checkbox, Tabs, Divider, message } from "antd";
import { Icon } from "../../controller/icon";

const menu = () => {
  return (
    <DropCss02>
      <Menu>
        <Menu.Item>
          <Checkbox className="check-style" />{" "}
          <div className="icon">
            {Icon("balcaoLogo", "", { width: "20px" })}
          </div>{" "}
          <spam className="spam-style">Balcão Rural</spam>
        </Menu.Item>
        <Menu.Item>
          <Checkbox className="check-style" />{" "}
          <div className="icon">{Icon("b2wLogo", "", { width: "20px" })}</div>{" "}
          <spam className="spam-style">B2W</spam>
        </Menu.Item>
        <Menu.Item>
          <Checkbox className="check-style" />{" "}
          <div className="icon">
            {Icon("cassolLogo", "", { width: "20px" })}
          </div>{" "}
          <spam className="spam-style">Cassol</spam>
        </Menu.Item>
        <Menu.Item>
          <Checkbox className="check-style" />{" "}
          <div className="icon">
            {Icon("carrefourLogo", "", { width: "20px" })}
          </div>{" "}
          <spam className="spam-style">Carrefour</spam>
        </Menu.Item>
        <Menu.Item>
          <Checkbox className="check-style" />{" "}
          <div className="icon">
            {Icon("magaluLogo", "", { width: "20px" })}
          </div>{" "}
          <spam className="spam-style">Magalu</spam>
        </Menu.Item>
        <Menu.Divider className="menu-divider" />
        <Menu.Item className="li-vermais">
          <div className="d-flex justify-content-between w-100">
            <spam>Ver todos</spam>{" "}
            <div className="icon-arrow">
              {Icon("arrowDefault", "", { width: "20px" })}
            </div>
          </div>
        </Menu.Item>
      </Menu>
    </DropCss02>
  );
};

const pedidos = (funct2) => {
  return (
    <DropCss03>
      <Menu>
        <Menu.Item onClick={funct2}>
          <Checkbox> Pendentes</Checkbox>
        </Menu.Item>
        <Menu.Item onClick={funct2}>
          <Checkbox> Pagos</Checkbox>
        </Menu.Item>
        <Menu.Item onClick={funct2}>
          <Checkbox> Faturados</Checkbox>
        </Menu.Item>
        <Menu.Item onClick={funct2}>
          <Checkbox> Enviados</Checkbox>
        </Menu.Item>
        <Menu.Item onClick={funct2}>
          <Checkbox> Concluidos</Checkbox>
        </Menu.Item>
        <Menu.Item onClick={funct2}>
          <Checkbox> Cancelados</Checkbox>
        </Menu.Item>
        <Menu.Divider className="menu-divider" />
        <Menu.Item className="li-vermais">
          <div className="d-flex justify-content-between w-100">
            <spam>Mais</spam>{" "}
            <div className="icon">
              {Icon("arrowDefault", "", { width: "20px" })}
            </div>
          </div>
        </Menu.Item>
      </Menu>
    </DropCss03>
  );
};

const points = (funct3) => {
  return (
    <DropCss03>
      <Menu>
        <Menu.Item>
          Ordenar por<strong>&ensp;data</strong>
          <div className="icon">{Icon("arrowDefault")}</div>
        </Menu.Item>
        <Menu.Item onClick={funct3}>Exibir em lista</Menu.Item>
        <Menu.Item>
          Escolher colunas <div className="icon">{Icon("arrowDefault")}</div>
        </Menu.Item>
        <Menu.Divider />
        <Menu.Item onClick={funct3} disabled="true">
          Pausar
        </Menu.Item>
        <Menu.Item onClick={funct3} disabled="true">
          Cancelar
        </Menu.Item>
        <Menu.Item onClick={funct3} disabled="true">
          Imprimir etiquetas
        </Menu.Item>
        <Menu.Item onClick={funct3} disabled="true">
          Reenviar notificações
        </Menu.Item>
        <Menu.Divider />
        <Menu.Item>
          Exportar <div className="icon">{Icon("arrowDefault")}</div>
        </Menu.Item>
      </Menu>
    </DropCss03>
  );
};

const { TabPane } = Tabs;

function callback(key) {
  console.log(key);
}

function confirm() {
  message.success('Excluído(s) com sucesso!');
}

/**
 * @param {Components} Parâmetro que espera Component para ser renderizado
 */
export default function Filtro({ drawerStatus, campos, Components, Modified, changeTabs }) {
  const [clas1, setClass1] = useState("none_width");
  const [clas, setClass] = useState("none-block");
  const [clas2, setClass2] = useState("none-block_icon");
  const [visible1, setVisible1] = useState(false);
  const [visible2, setVisible2] = useState(false);
  const [color, setColor] = useState("color-item");

  useEffect(() => {
    if (visible1) {
      setVisible2(false)
    }
  }, [visible1])

  useEffect(() => {
    if (visible2) {
      setVisible1(false)
    }
  }, [visible2])

  function hasClass(params) {
    setClass("block");
    setClass2("show_icon");
  }
  const showModal = () => {
    setIsModalVisible(true);
  };
  function selectColor() {
    setColor("select-color");
  }

  function hasClass2() {
    setClass1("show_width");
  }
  function desableClas() {
    setClass("");
    hasClass2("");
    setClass2("");
    setVisible2(false);
  }
  function visible() {
    setVisible1(!visible1);
    if (visible1) {
      desableClas();
    } else {
      hasClass();
    }
  }
  function visible_t() {
    setVisible2(!visible2);
    if (visible2) {
      desableClas();
    } else {
      hasClass();
    }
  }

  return (
    <FiltroCss>
      <div className={`all ${Modified}`}>
        <div className="itemsFilter">
          <div className="iconsDiv">
            {campos.map((i) => {
              if (i.type === "filter") {
                return (
                  <div className={i.className} onChange={color}>
                    {Icon("FilterOutline", "", {
                      width: "20px",
                    })}
                  </div>
                );
              }
              if (i.type === "plus") {
                return (
                  <div className={i.className} onChange={color} onClick={i.click}>
                    {Icon("plusLogo", "", {
                      width: "20px",
                    })}
                  </div>
                );
              }
              if (i.type === "editLogo") {
                return (
                  <div className={i.className} onChange={color}>
                    {Icon("editLogo", "", {
                      width: "20px",
                    })}
                  </div>
                );
              }
              if (i.type === "escLogo") {
                return (
                  <div className={i.className} onChange={color}>
                    {Icon("escIcon", "", {
                      width: "20px",
                    })}
                  </div>
                );
              }
              if (i.type === "checked") {
                return (
                  <div className={i.className} onChange={color}>
                    {Icon("checkedBox", "", {
                      width: "16x",
                    })}
                  </div>
                );
              }
              if (i.type === "uncheck") {
                return (
                  <div className={i.className} onChange={color}>
                    {Icon("uncheckBox", "", {
                      width: "16x",
                    })}
                  </div>
                );
              }
              if (i.type === "change-icon") {
                return (
                  <div className={i.className} onChange={color}>
                    {Icon("changeIcon", "", {
                      width: "16px",
                    })}
                  </div>
                );
              }
              if (i.type === "plane") {
                return (
                  <div className={i.className} onChange={color}>
                    {Icon(i.icon, "", {
                      width: "20px",
                    })}
                    {i.dividerSrc && (
                      <Divider
                        className={i.dividerStyle}
                        type={i.dividerDirection}
                      />
                    )}
                  </div>

                );
              }
              if (i.type === "trash") {
                return (

                  <div className={i.className} onClick={i.click} onChange={color}>
                    {Icon(i.icon, "", {
                      width: "24px",
                    })}
                    {i.dividerSrc && (
                      <Divider
                        className={i.dividerStyle}
                        type={i.dividerDirection}
                      />
                    )}
                  </div>
                );
              }
              if (i.type === "field-date") {
                return (
                  <div className="campo-date">
                    <label><strong>Data da venda </strong></label>
                    <input className="data" type="datetime-local" />
                  </div>
                );
              }
              if (i.type === "back") {
                return (
                  <Link href={i.to}>
                    <div onClick={selectColor} className={i.className}>
                      {Icon(i.icon, "", {
                        width: "24px",
                        heigth: "24px",
                      })}
                    </div>
                  </Link>
                );
              }
              if (i.type === "divider") {
                return <Divider className={i.className} type={i.direction} />;
              }
              if (i.type === "points") {
                return (
                  <Dropdown
                    overlay={() => points(desableClas)}
                    trigger={["click"]}
                  >
                    <a
                      className={`points-dropdown ${Modified}`}
                      onClick={hasClass}
                    >
                      <div className={i.className}>
                        {Icon("MoreOutlined", "", {
                          width: "20px",
                          heigth: "20px",
                        })}
                      </div>
                    </a>
                  </Dropdown>
                );
              }
              if (i.type === "input") {
                return (
                  <div className={i.className}>
                    <Input
                      lista="items"
                      prefix={Icon("Search", "", {
                        width: "24px",
                      })}
                      className="input"
                    />
                    {i.escSrc && (
                      <div className={i.classNameEsc}>
                      {Icon("escIcon", "", {
                        width: "20px",
                      })}
                    </div>
                    )}
                    {i.dividerSrc && (
                      <Divider
                        className={`input-divider ${i.dividerStyle}`}
                        type={i.dividerDirection}
                      />
                    )}
                  </div>
                );
              }
            })}
          </div>
          <div className="text-filter d-flex">
            {campos.map((i) => {
              if (i.type === "tabs") {
                return (
                  <div className="detailsFilter">
                    <Tabs
                      defaultActiveKey="1"
                      type="line"
                      onChange={changeTabs}
                    >
                      {i.menus.map((Tamp) => {
                        return (
                          <TabPane
                            tab={Tamp.label}
                            key={Tamp.label}
                            disabled={Tamp.disabled}
                          >
                            {/* <div className="p-absolute">{Tamp.Component}</div> */}
                          </TabPane>
                        );
                      })}
                    </Tabs>
                  </div>
                );
              }
              if (typeof i.onDrawer != undefined) {
                if (i.type === "icon-down-mkt" && i.onDrawer == drawerStatus) {
                  return (
                    (<i.icon className={i.class} />),
                    (
                      <Dropdown
                        overlay={() => menu(desableClas)}
                        trigger={["click"]}
                        onClick={visible}
                        visible={visible1}
                      >
                        <a
                          className="ant-dropdown-link"
                          onClick={(e) => e.preventDefault()}
                        >
                          <p className={`none ${clas}`}>
                            {i.dropOne}{" "}
                            <div className="div-arrow-down">
                              {Icon("arrowDown", "", {
                                width: "14px",
                              })}
                            </div>
                          </p>
                        </a>
                      </Dropdown>
                    )
                  );
                }
                if (i.type === "dividerTextFilter" && i.onDrawer == drawerStatus) {
                  return <Divider className={`${i.className} none_110 ${clas}`} type={i.direction} />;
                }
                if (i.type === "icon-down" && i.onDrawer == drawerStatus) {
                  return (
                    (<i.icon className={i.class} />),
                    (
                      <Dropdown
                        overlay={() => pedidos()}
                        trigger={["click"]}
                        onClick={visible_t}
                        visible={visible2}
                      >
                        <a
                          className="ant-dropdown-link"
                          onClick={(e) => e.preventDefault()}
                        >
                          <p>
                            <span className={`none_110 ${clas}`}>Todos os</span>
                            <b>{i.value}</b> {i.dropTwo}{" "}
                          </p>
                          <div className="div-arrow-down">
                            {Icon("arrowDown", "")}
                          </div>
                        </a>
                      </Dropdown>
                    )
                  );
                }
              } else {
                if (i.type === "icon-down-mkt") {
                  return (
                    (<i.icon className={i.class} />),
                    (
                      <Dropdown
                        overlay={() => menu(desableClas)}
                        trigger={["click"]}
                        onClick={visible}
                        visible={visible1}
                      >
                        <a
                          className="ant-dropdown-link"
                          onClick={(e) => e.preventDefault()}
                        >
                          <p className={`none ${clas}`}>
                            {i.dropOne}{" "}
                            <div className="div-arrow-down">
                              {Icon("arrowDown", "", {
                                width: "14px",
                              })}
                            </div>
                          </p>
                        </a>
                      </Dropdown>
                    )
                  );
                }
                if (i.type === "dividerTextFilter") {
                  return <Divider className={`${i.className} none_110 ${clas}`} type={i.direction} />;
                }
                if (i.type === "icon-down") {
                  return (
                    (<i.icon className={i.class} />),
                    (
                      <Dropdown
                        overlay={() => pedidos()}
                        trigger={["click"]}
                        onClick={visible_t}
                        visible={visible2}
                      >
                        <a
                          className="ant-dropdown-link"
                          onClick={(e) => e.preventDefault()}
                        >
                          <p>
                            <span className={`none_110 ${clas}`}>Todos os</span>
                            <b>{i.value}</b> {i.dropTwo}{" "}
                          </p>
                          <div className="div-arrow-down">
                            {Icon("arrowDown", "")}
                          </div>
                        </a>
                      </Dropdown>
                    )
                  );
                }
              }
            })}
          </div>
        </div>
        {campos.map((i) => {
          if (i.divider === true) {
            return <Divider className="dividerStyle" />;
          }
        })}
      </div>
    </FiltroCss>
  );
}
