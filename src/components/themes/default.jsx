/* eslint-disable react-hooks/rules-of-hooks */
import React, { useState, useEffect } from "react";
import { Layout, Menu, Row, Col, Dropdown } from "antd";
import { MenuUnfoldOutlined, MenuFoldOutlined } from "@ant-design/icons";
import Link from "next/dist/client/link";
import Styled from "../../styles/components/themes/default"
import DropHeader from "../../styles/components/filtros/dropheader"
import { savePage } from "../../controller/pages";
import { Icon } from "../../controller/icon";
import DropCss from "../../styles/components/filtros/dropdown";
import Footer from "../../components/footer/rodape";
import Router from "next/router";
const { Header, Sider, Content } = Layout;
/**
 * @param {Components} Parâmetro que espera Component para ser renderizado
 * @param {MenuJson} Parâmetro que espera json para montar menu lateral
 * @return <componente a ser renderizado/>
 */
function layoutDefault({ Components, MenuJson }) {
  const [collapsed, setCollapsed] = useState(false);
  const [currentSubMenu, setCurrentSubMenu] = useState("");
  const [load, setLoad] = useState(true);
  const [celetor, setCeletor] = useState("1");

  useEffect(() => {
    for (let i = 0; i < MenuJson.menuCategorias.length; i++) {
      if (MenuJson.menuCategorias[i].to == window.location.pathname) {
        setCurrentSubMenu(MenuJson.menuCategorias[i].item);
        break;
      } else if (MenuJson.menuCategorias[i].extraPages) {
        for (let i2 = 0; i2 < MenuJson.menuCategorias[i].extraPages.length; i2++) {
          if (MenuJson.menuCategorias[i].extraPages[i2].to == window.location.pathname) {
            setCurrentSubMenu(MenuJson.menuCategorias[i].item);
            break;
          }
        }
      }
    }
  }, []);

  useEffect(() => {
    setLoad(true);

    let path = window.location.pathname;

    path = path.split("/");
    let key = MenuJson.MenuJson.filter((menu) => menu.to.includes(path[2]));
    setCeletor(key[0] && key[0].key);
    console.log(key[0], MenuJson);
    key[0] = {
      key: key[0],
      to: window.location.pathname,
      item: key[0],
    };
    savePage(key[0] && key[0]);
    setLoad(false);
  }, [MenuJson]);

  function detectComponent() {
    return <Components />;
  }
  let toggle = () => {
    setCollapsed(!collapsed);
  };

  const menu = (
    <DropCss>
      <Menu className="user">
        <Menu.Item key="0">
          <h2>
            <i>
              <b>HUBBA</b>
            </i>
          </h2>
        </Menu.Item>
        <Menu.Divider />
        <Menu.Item key="1">Minhas informações</Menu.Item>
        <Menu.Item key="2">
          Densidade <div className="icon">{Icon("arrowDefault")} </div>
        </Menu.Item>
        <Menu.Item key="2">Preferências</Menu.Item>
        <Menu.Divider />
        <Menu.Item key="3">Sobre o Hubba</Menu.Item>
        <Menu.Divider />
        <Menu.Item key="3"><Link href="login">Sair </Link></Menu.Item>
      </Menu>
    </DropCss>
  );

  const dropHeader = (
    <DropHeader>
      <Menu>
        <Menu.Item key="1">
          WebContinental
        </Menu.Item>
        <Menu.Item key="2">
          Continental Center
        </Menu.Item>
      </Menu>
    </DropHeader>
  );

  const { SubMenu } = Menu;

  const redirect = (to) => {
    Router.push(to);
  };
  return (
    <>
      {
        load ? (
          <div className="load">AGUARDE...</div>
        ) : (
          <Styled>
            <Layout>
              <Header className="site-layout-background">
                <Row className="custom-header">
                  <Col span={16} xs={24} md={16} className="d-flex">
                    {React.createElement(
                      collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                      {
                        className: "trigger",
                        onClick: toggle,
                      }
                    )}

                    {Icon("menuIcon", "", {
                      width: "20px",
                      heigth: "14px",
                    })}{" "}
                    <Dropdown
                      overlay={dropHeader}
                      trigger={["click"]}
                    >
                      <div className="d-flex">
                        <b>Web Continental</b>{" "}
                        {Icon("arrowDown", "", {
                          width: "13.31px",
                          heigth: "13.31px",
                        })}
                      </div>
                    </Dropdown>
                  </Col>
                  <Col
                    span={16}
                    xs={24}
                    md={8}
                    className="d-flex align-items-center justify-content-right menu-right"
                  >
                    <div className="chat-icon">
                      {Icon("chatIcon", "", {
                        width: "18px",
                        heigth: "16px",
                      })}
                    </div>
                    <div className="vertical-1" />
                    <div className="vertical-2" />
                    <div className="notification-icon">
                      {Icon("notificationIcon", "", {
                        width: "16px",
                        heigth: "18px",
                      })}
                    </div>
                    <div>
                      <small>Franciele</small>
                    </div>
                    <div className="user-icon">
                      <Dropdown overlay={menu} trigger={["click"]}>
                        {Icon("userIcon", "", {
                          width: "16px",
                          heigth: "18px",
                        })}
                      </Dropdown>
                    </div>
                  </Col>
                </Row>
              </Header>
              <Layout>
                <Sider
                  breakpoint="lg"
                  collapsed={collapsed}
                  trigger={null}
                  collapsible
                  theme="light"
                >
                  <Menu
                    className="menu"
                    theme="light"
                    mode="vertical"
                    defaultSelectedKeys={[celetor]}
                  >
                    <p>{MenuJson.title}</p>
                    <hr />
                    {MenuJson.MenuJson.map((json) => {
                      return (
                        json.item == "Cadastros"
                          ?
                          <SubMenu title={currentSubMenu != "" ? `${json.item}: ${currentSubMenu}` : json.item} mode="vertical" className={"submenu", currentSubMenu != "" ? "ant-menu-item-selected" : ""}>
                            {MenuJson.menuCategorias.map(catego => {
                              return (
                                <Menu.Item key={catego.key} onClick={(e) => {
                                  redirect(catego.to)
                                }}>
                                  {catego.item}
                                </Menu.Item>
                              )
                            })}
                          </SubMenu>
                          : <Menu.Item key={json.key} onClick={() => {
                            redirect(json.to)
                          }}>
                            {json.item !== "Login" ? json.item : null}
                          </Menu.Item>
                      );
                    })}
                    {/* {MenuJson.MenuJson.map(json => {
                    return (
                      json.item == "Cadastro: categorias" ?
                        <SubMenu key={json.key}>
                          <Menu.Item key={json.key}>{json.item.extraItems}</Menu.Item>
                        </SubMenu> : ""
                    )
                  })} */}
                    <h2 className="identidade">{MenuJson.header}</h2>
                  </Menu>
                </Sider>
                <Content>
                  <div className="site-layout-background">
                    {detectComponent()}
                  </div>
                </Content>
              </Layout>
            </Layout>
          </Styled>
        )}
      <Footer />
    </>
  );
}
export default layoutDefault;
