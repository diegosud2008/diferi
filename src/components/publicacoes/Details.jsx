import React from "react";
import { Collapse, Alert, Button, Row, Col } from "antd";
import Table from "../../components/global/Table"
import FormPerson from "../global/FormPerson";
import { Icon } from "../../controller/icon";
import DetailsCss from "../../styles/components/pedidos/details";

const dataTable = require("../../variables/pedidos/dataTable.json");
const columnsTable = require("../../variables/pedidos/columnsTable.json");
const headerTable = false;

const infos = {
  collapseTiltle: "Informações gerais",
  camposConfig1: [
    {
      type: "input",
      label: "Marca",
      value: "Ventisol",
      className: "inputStyle",
      desable: true,
    },
    {
      type: "input",
      label: "SKU",
      value: "MKP000219000651",
      className: "skuStyle",
      desable: true,
    },
    {
      type: "input",
      label: "Código de barras",
      value: "78945612",
      className: "skuStyle",
      desable: true,
    },
    {
      type: "input",
      label: "NBM",
      className: "skuStyle",
      value: "78945612",
      desable: true,
    },
    {
      type: "select",
      mode: "",
      desable: false,
      label: "Origem",
      className: "skuStyle",
      allowClear: false,
      defaultValue: "Nacional",
      options: [
        {
          value: "teste",
          text: "teste",
        },
        {
          value: "disabled",
          text: "disabled",
        },
      ],
    },
  ],
};
const infos2 = {
  collapseTiltle:"Marketplaces"
}
const { Panel } = Collapse;
const onClose = (e) => {
  console.log(e, "I was closed.");
};

const columns = columnsTable.columnsTable.map((temp) => ({
  ...temp,
  render: (text, record) => <div>{text}</div>,
}));

const data = dataTable.dataTable.map((temp) => ({
  ...temp,
  icon: Icon(temp.icon),
  action: temp.action.map((tempAction) => {
    return (
      <div className="d-flex align-items-center align justify-content-between">
        <strong>{tempAction.price}</strong>
        <a
          href={`${tempAction.to}/${temp.id}`}
          className={tempAction.className}
        >
          {Icon(tempAction.icon, "", { width: "24px", heigth: "24px" })}
        </a>
      </div>
    );
  }),
}));

function Details({ props }) {
  const [visibles, setVisibles] = React.useState(false);
  const [className, setClassName] = React.useState("tamanho_100");
  const [clasActive, setClasActive] = React.useState(false);
  const [none, setNone] = React.useState("d-none");
  const [pedidos, setPedido] = React.useState();
  function visbles(pedido, index) {
    setVisibles(!visibles);

    [...document.getElementsByClassName(`active`)].map((active) =>
      active.classList.remove("active")
    );
    setTimeout(() => {
      if (!visibles || pedido != pedidos) {
        setNone("d-block");
        setClassName("tamanho_70");
        setVisibles(true);
        [...document.getElementsByClassName(`row_${index}`)][0].classList.add(
          "active"
        );
        setClasActive(index);
      } else {
        setNone("d-none");
        setClassName("tamanho_100");
        setClasActive(false);
      }
      setPedido(pedido);
    }, 200);
  }

  function onClose() {
    setVisibles(false);
    setTimeout(() => {
      setNone("d-none");
      setClassName("tamanho_100");
    }, 200);
  }
  return (
    <DetailsCss>
      <div className="">
        <div className="headerDetalhesProdutos">
          <div className="divHeaderDetalhesProdutos">
            <div id="titleHeaderDetalhesProdutos">
              <h1 className="h1HeaderDetalhesProdutos">
                Aquecedor doméstico halógeno AH-01 127 V premium
              </h1>
            </div>
          </div>
        </div>
        <Collapse
          className="site-layout-background collapseStyle"
          bordered={false}
          defaultActiveKey={["1"]}
        >
          <Panel className="font-20" header={infos.collapseTiltle} key="1">
                <div className="">
                {infos.camposConfig1.map((config) => {
                  return (
                    <FormPerson
                      config={config}
                      classForm="inputStyle"
                      options={config.options}
                    />
                  );
                })}
                </div>
          </Panel>
          <Panel className="font-20" header={infos2.collapseTiltle} key="2">
                <Table
                dataTable={data}
                columnsTable={columns}
                headerTable={headerTable}
                visbles={visbles}
                selectPedidos={pedidos}/>
          </Panel>
        </Collapse>
      </div>
    </DetailsCss>
  );
}

export default Details;
