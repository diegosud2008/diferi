import React, { useState } from "react";
import { Drawer, Collapse, Descriptions } from "antd";
import { Icon } from "../../controller/icon";
import { UpOutlined } from "@ant-design/icons";
import {Styles, StylesDrawer} from "../../styles/components/pedidos/drawer";

const { Panel } = Collapse;

function MainDrawer({ vesibles, onClose, drawerJson }) {
  const title = () => {
    return (
      <Styles>
        <div className="title">
          {drawerJson.icone &&
            Icon(drawerJson.icone, "", { width: "24px", heigth: "24px" })}
          {drawerJson.pedidos && (
            <div>
              <strong>{drawerJson.title}</strong>
              <spam>{drawerJson.id_pedido}</spam>
            </div>
          )}
          {drawerJson.produtos && (
            <div className="div-header">
              {" "}
              <div className="div-text">
                <p className="titleProdutos">
                  <strong>{drawerJson.tipo}</strong> {drawerJson.title}
                </p>
              </div>
              <div className="div-img">
                {drawerJson.image && (
                  <div className="div-int-img">
                    <img className="styleImage" src={drawerJson.image}></img>
                  </div>
                )}{" "}
              </div>
            </div>
          )}
        </div>
      </Styles>
    );
  };
  return (
    <StylesDrawer>
      <Drawer
        title={title()}
        style={{ position: "absolute" }}
        width={"100%"}
        placement="right"
        visible={vesibles}
        getContainer={false}
        closable={true}
        headerStyle={{
          background: "#F3F5F9",
          position: "relative",
        }}
        bodyStyle={{ width: "89%" }}
        onClose={onClose}
      >
        {/* <div className="close" onClick={ onClose }>{ Icon('closeIcon') }</div> */}
        <Collapse
          expandIconPosition="right"
          expandIcon={({ isActive }) => (
            <UpOutlined twoToneColor="#4466AB" rotate={isActive ? 180 : 0} />
          )}
        >
          {drawerJson.dados.map((json, index) => {
            return (
              <Panel
                header={json.collapseTitle}
                key={index}
                extra={Icon("linkOutlineGray", "", {
                  width: "16px",
                  heigth: "16px",
                })}
              >
                {drawerJson.dados[index].clientJson.map((json, index) => {
                  return (
                    <div className="d-flex justify-content-between pt-1">
                      <div className="spamLabel">
                        {json.icon &&
                          Icon(json.icon, "", {
                            width: "24px",
                            heigth: "24px",
                          })}
                        <spam className={`${json.classLabel}`}>
                          {json.label}
                        </spam>
                      </div>
                      <div className="spamValue">
                        <spam className={`${json.classValue}`}>
                          {json.value}
                        </spam>
                      </div>
                    </div>
                  );
                })}
              </Panel>
            );
          })}
        </Collapse>
      </Drawer>
    </StylesDrawer>
  );
}
export default MainDrawer;
