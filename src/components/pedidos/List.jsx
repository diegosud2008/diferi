/* eslint-disable react-hooks/rules-of-hooks */
import React from "react";
import Link from "next/link";
import { List, Tooltip } from "antd";
import { Icon } from "../../controller/icon";
import StyleList from "../../styles/components/pedidos/list";

/**
 * @param {ListJson} Parâmetro que espera json para montar lista
 * @return <componente a ser renderizado/>
 **/
function MainList({ ListJson, visbles, ColunmModified }) {
  const [visibles, setVisibles] = React.useState(false);
  let data = ListJson;
  function link(condition, icon) {
    if (condition) {
      return (
        <Link href={condition}>
          <div className="icons">
            {Icon(icon, "d-flex align-items-center", {
              width: "20px",
              heigth: "20px",
              color: "#4466AB",
            })}
          </div>
        </Link>
      );
    }
    return (
      <div className="icons">
        {Icon(icon, "d-flex align-items-center", {
          width: "20px",
          heigth: "20px",
          color: "#4466AB",
        })}
      </div>
    );
  }
  return (
    <List
      size="large"
      itemLayout="horizontal"
      dataSource={data}
      renderItem={(item) => (
        <StyleList>
          <List.Item onClick={() => visbles(item.pedido)}>
            <Tooltip placement="bottom" title="Pedido cancelado">
              <span className="dot mv-auto "></span>
            </Tooltip>
            <List.Item.Meta
              className="mv-auto"
              avatar={Icon(item.photo)}
              title={<span>{item.title}</span>}
              description={item.descripition}
            />
            <div
              className={`d-flex justify-content-between w-15 ${ColunmModified}`}
            >
              {item.price && (
                <div className="d-flex align-items-center">
                  <b>{item.price}</b>
                </div>
              )}
              {item.eye && link(item.to, item.icon)}
              {item.date && (
                <div
                  className={`d-flex align-items-center ml-1 ${ColunmModified}`}
                >
                  {item.date}
                </div>
              )}
              {item.pedido && (
                <div
                  className={`d-flex align-items-center ml-1 ${ColunmModified}`}
                >
                  {item.pedido}
                </div>
              )}
            </div>
          </List.Item>
        </StyleList>
      )}
    />
  );
}
export default MainList;
