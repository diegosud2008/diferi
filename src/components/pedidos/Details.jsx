import React, { useState } from "react";
import { Collapse, Descriptions, Steps } from "antd";
import DetailsCss from "../../styles/components/pedidos/details";

const { Panel } = Collapse;
const { Step } = Steps;

function Details({ infoJson, clientJson, payJson }) {
  return (
    <DetailsCss>
      <div className="p-2 m-auto" style="overflow: hidden !important">
        <div className="header-details">
          <h1 className="font-20">
            Fernanda Monteiro Borges da Silva Medeiros
          </h1>
          <spam className="c-red">
            <b>Cancelado</b>
          </spam>
          <div className="timeline">
            <div className="line"></div>
            <Steps size="default" current={1}>
              <Step title="Pendente" />
              <Step title="Pago" />
              <Step title="Faturado" />
              <Step title="Enviado" />
              <Step title="Concluído" />
            </Steps>
          </div>
        </div>
        <Collapse
          className="site-layout-background"
          bordered={false}
          defaultActiveKey={["1"]}
        >
          <Panel className="font-20" header={infoJson.collapseTitle} key="1">
            <Descriptions layout="vertical" bordered={false}>
              {infoJson.infoJson.map((json, index) => {
                return (
                  <Descriptions.Item label={json.label} key={index}>
                    {json.value}
                  </Descriptions.Item>
                );
              })}
            </Descriptions>
          </Panel>
          <Panel className="font-20" header={clientJson.collapseTitle} key="2">
            <Descriptions layout="vertical" bordered={false}>
              {clientJson.clientJson.map((json, index) => {
                return (
                  <Descriptions.Item label={json.label} key={index}>
                    {json.value}
                  </Descriptions.Item>
                );
              })}
            </Descriptions>
          </Panel>
          <Panel className="font-20" header={payJson.collapseTitle} key="3">
            <Descriptions layout="vertical" bordered={false}>
              {payJson.payJson.map((json, index) => {
                return (
                  <Descriptions.Item label={json.label} key={index}>
                    {json.value}
                  </Descriptions.Item>
                );
              })}
            </Descriptions>
          </Panel>
        </Collapse>
      </div>
    </DetailsCss>
  );
}

export default Details;
