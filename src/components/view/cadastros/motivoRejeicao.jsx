import React, { useEffect, useState } from "react";
import Filter from "../../../components/filtros/Filtro"
import Table from "../../../components/global/TableManutencao";
import { Modal, Form, Checkbox, Input, Empty, Popconfirm, message } from "antd"
import Styles from "../../../styles/components/categorias/editRejeicao";
import { TableData } from "../../../controller/pages/pedidos";
import { Icon } from "../../../controller/icon"

const columnsTable = require("../../../variables/cadastro/categorias/motivoRejeicao.json/columnsTable.json");
const columns = columnsTable.columnsTable.map((temp) => ({
  ...temp,
  render: (text, record) => <div>{text}</div>,
}));
const jsonDefault = [
  {
    icon: "plusLogo",
    type: "plus",
    className: "plusIcon",
  },
  {
    icon: "SearchOutlined",
    className: "srcIconProd",
    type: "none",
  },
  {
    input: "Search",
    className: "srcIconPed",
    type: "input",
  },
];
export function MotivoRejeicao() {
  const [data, setData] = React.useState([]);
  const [dados, setDados] = React.useState({})
  const headerTable = false;
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [jsonFilter, setJsonFilter] = React.useState(jsonDefault);
  const [currentPage, setCurrentPage] = React.useState(1);
  let [itemsDelete, setitemsDelete] = useState([])
  const [selected, setSelected] = useState('unselected')


  const showModal = (dados) => {
    setIsModalVisible(true);
    setDados(dados);
  };

  const handleOk = (id) => {
    setIsModalVisible(false);
    let insercao = data[data.findIndex(temp => temp.id === id)]
    insercao.motivo = dados.motivo
    insercao.ativo = dados.ativo
    console.log(data)
  };

  function visbles(index, id, items) {
    index.className === "unselected" ? setSelected(index.className = 'selected') : setSelected(index.className = 'unselected');
    console.log(selected, index.id)
    itemsDelete += itemsDelete.length == 0 ? index.ativo : "," + index.ativo
    setitemsDelete(itemsDelete);
  }

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  useEffect(async (index) => {
    let data = await TableData(currentPage);

    data = data.map((temp) => ({
      ...temp,
      icon: <div className="editIcon" onClick={() => showModal(temp)}>{Icon(temp.icon)}</div>,
      trash: <Popconfirm placement="bottom" onConfirm={deleteItem} title={`Deseja excluir o motivo "${temp.motivo}" ?`} okText="Excluir" cancelText="Cancelar"><div className="trashIcon">{Icon(temp.trash)}</div></Popconfirm>,
    }));
    let json = jsonFilter.map((item, index) => {
      return {
        ...item,
        click: () => showModal(item)
      }
    })
    setJsonFilter(json)
    setData(data);
    setitemsDelete(itemsDelete);
    if (data.length > 0) {
      useScrollInfity(data);
    }
  }, []);

  function useScrollInfity(list) {
    const intersectionObserver = new IntersectionObserver(async (entries) => {
      if (entries.some((entry) => entry.isIntersecting)) {
        let datas = await TableData(currentPage);
        datas = datas.map((temp) => ({
          ...temp,
          icon: <div className="editIcon" onClick={() => showModal(temp)}>{Icon(temp.icon)}</div>,
          trash: <Popconfirm placement="bottom" onConfirm={deleteItem} title={`Deseja excluir o motivo "${temp.motivo}" ?`} okText="Excluir" cancelText="Cancelar"><div className="trashIcon">{Icon(temp.trash)}</div></Popconfirm>,
        }));
        setData((data) => { return [...data, ...datas] });
      }
    })
    intersectionObserver.observe(document.querySelector(`#listEnd`))
  }

  function alterItem(value) {
    let dado = {
      ...dados,
      motivo: value.target.value,
    }
    setDados(dado)
  }

  function checkedAction(e) {
    let dado = {
      ...dados,
      ativo: e.target.checked == true ? "Ativo" : "Inativo"
    }
    setDados(dado);
  }

  function deleteItem() {
    itemsDelete = "Inativo"
    message.success(`excluído(s) com sucesso!`);
    let dado = {
      ...dados,
      ativo: itemsDelete
    }
    setDados(dado)
    console.log(dado)
  }

  return (
    <Styles>
      <div className="container">
        <Filter campos={jsonFilter} />
        {
          data == "" ?
            <div className='spinner' />
            :

            data.length === 0 ?
              <Empty description={<span>Sem Dados</span>} />
              :
              <>
                <Table
                  data_table={data}
                  columns_table={columns}
                  header_table={headerTable}
                  visbles={visbles}
                />
                <span id="listEnd" />
                  <Modal
                    title="Edição de motivo"
                    visible={isModalVisible}
                    cancelText="Cancelar"
                    okText="Salvar"
                    onOk={() => handleOk(dados.id)}
                    onCancel={handleCancel}
                  >
                    <Form
                      labelCol={{ span: 4 }}
                      wrapperCol={{ span: 14 }}
                      layout="horizontal"
                    >
                      <Form.Item label="Motivo">
                        <Input className="value-input" value={dados.motivo} onChange={(value) => alterItem(value)} />
                      </Form.Item>
                      <Form.Item label="Ativo">
                        <Checkbox onChange={(checked) => checkedAction(checked)} checked={dados.ativo == "Ativo" ? true : false} />
                      </Form.Item>
                    </Form>
                  </Modal>
              </>
        }
      </div>
    </Styles>
  )
}