import React, { useState } from "react";
import Filtro from "../../filtros/Filtro";
import Tree from "../../global/Tree";
import Styles from "../../../styles/components/categorias/categorias";
import Drawer from "../../global/Drawer";

const drawerJson = require("../../../variables/cadastro/categorias/drawer.json");
const treeData = require("../../../variables/cadastro/categorias/treeDataInfo.json");
const jsonDefault = [
  {
    divider: false,
  },
  {
    icon: "plusLogo",
    type: "plus",
    className: "planeIcon",
  },
  {
    icon: "editLogo",
    type: "editLogo",
    className: "planeIcon",
  },
  {
    type: "divider",
    direction: "vertical",
    className: "divider-style",
  },
  {
    icon: "FilterOutlined",
    className: "filterIconProd",
    type: "filter",
  },
  {
    icon: "SearchOutlined",
    className: "srcIconProd",
    type: "none",
  },
  {
    input: "Search",
    className: "srcIconPed",
    type: "input",
  },
  {
    type: "divider",
    direction: "vertical",
    className: "divider-style mL-37",
  },
  {
    type: "uncheck",
    className: "checkBox",
  },
  {
    type: "divider",
    direction: "vertical",
    className: "divider-style mL-37",
  },
  {
    icon: "MoreOutlined",
    className: "list-icon mL-37",
    type: "points",
  },
  {
    class: "down-icon",
    type: "icon-down",
    dropOne: "Todos os marketplaces",
    dropTwo: "categorias",
    value: "1.308",
  },
];

export default function categorias() {
  const [none, setNone] = React.useState("d-none");
  const [className, setClassName] = React.useState("tamanho_100");
  const [visibles, setVisibles] = React.useState(false);
  const onSelect = (selectedKeys, info) => {
    console.log(selectedKeys, info);
    document.querySelectorAll(".ant-tree-treenode")[0].style.display = "none";
    if (info.selected) {
      setVisibles(true);
      setNone("d-block");
      setClassName("tamanho_70");
    } else {
      onClose()
    }
  }
  function onClose() {
    setVisibles(false);
      setNone("d-none");
      setClassName("tamanho_100");
  }
  return (
    <Styles>
      <div className={className}>
        <Filtro campos={jsonDefault} />
        <Tree treeData={treeData} onSelect={onSelect}/>
      </div>
      <div className={`site-drawer-render-in-current-wrapper ${none}`}>
        <Drawer vesibles={visibles} onClose={onClose} drawerJson={drawerJson} />
      </div>
    </Styles>
  );
}
