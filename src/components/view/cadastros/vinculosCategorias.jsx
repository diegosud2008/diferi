import React, { useState, useEffect } from "react";
import Filtro from "../../filtros/Filtro";
import StyleVinculo from "../../../styles/components/global/vinculoCategorias";
import Tree from "../../global/Tree";
import { Icon } from "../../../controller/icon";
import itensList from "../../../variables/cadastro/categorias/itensList.json";

const treeData = require("../../../variables/cadastro/categorias/treeDataInfoVinculo.json");

const jsonDefault = [
  {
    divider: false,
  },
  {
    icon: "SearchOutlined",
    className: "srcIconProd",
    type: "none",
  },
  {
    input: "Search",
    dividerSrc: false,
    className: "srcIconPed",
    type: "input",
  },
];

export default function vinculos() {
  const [currentActive, setCurrentActive] = useState("")
  const [currentMkp, setCurrentMkp] = useState("")
  function handleClass(event, classActive, mkp) {
    setCurrentMkp(mkp)
    if (currentActive != "") {
      if (currentActive == classActive) {
        document.getElementsByClassName(currentActive)[0].classList.remove("active")
        setCurrentMkp("")
        setCurrentActive("")
      } else {
        document.getElementsByClassName(currentActive)[0].classList.remove("active")
        document.getElementsByClassName(classActive)[0].classList.add("active")
        setCurrentActive(classActive)
      }
    } else {
      document.getElementsByClassName(classActive)[0].classList.add("active")
      setCurrentActive(classActive)
    }

  }
  return (
    <StyleVinculo>
      <div className="main-div">
        <div className="header-vinculo">
          Vinculando subcategoria &nbsp;<strong>chopeira</strong>
        </div>
        <div className="category-wrapper">
          <div className="body-vinculo">
            <div className="div-mkt-list">
              {itensList.itensList.map((items, index) => {
                return (
                  <div
                    className={"item-mkt-list-" + index}
                    key={index}
                    onClick={(event) => {
                      handleClass(event, "item-mkt-list-" + index, items.title);
                    }}
                  >
                    {Icon(items.icon, "", { width: "20px" })}
                    <p className="title-mkt-list">{items.title}</p>
                  </div>
                );
              })}
            </div>

          </div>
          <div className="tree-wrapper" style={{ display: currentMkp != "" ? "block" : "none" }}>
            <Filtro campos={jsonDefault} />
            <Tree treeData={treeData} />
          </div>
        </div>
      </div>
    </StyleVinculo>
  );
}
