import React, { useEffect } from "react";
import Filtro from "../../filtros/Filtro";
import Table from "../../global/Table";
import Drawer from "../../pedidos/Drawer";
import Styles from "../../../styles/components/publicacoes/default";
import { Icon } from "../../../controller/icon";
import { TableData } from "../../../controller/pages/publicacoes";

const drawerJson = require("../../../variables/publicacoes/drawer/drawerJson.json");
const dataTable = require("../../../variables/publicacoes/dataTable.json");
const columnsTable = require("../../../variables/publicacoes/columnsTable.json");
const headerTable = false;

import { Tooltip } from "antd";

const jsonDefault = [
  {
    divider: false,
  },
  {
    icon: "plusLogo",
    type: "plus",
    className: "planeIcon",
  },
  {
    type: "divider",
    direction: "vertical",
    className: "divider-style",
  },
  {
    icon: "FilterOutlined",
    className: "filterIconProd",
    type: "filter",
  },
  {
    icon: "SearchOutlined",
    className: "srcIconProd",
    type: "none",
  },
  {
    input: "Search",
    className: "srcIconPubli",
    type: "input",
  },
  {
    type:"checked",
    className: "checked"
  },
  {
    type: "divider",
    direction: "vertical",
    className: "divider-style mL-37",
  },
  {
    className: "change-icon",
    type: "change-icon",
  },
  {
    icon: "MoreOutlined",
    className: "list-icon mL-37",
    type: "points",
  },
  {
    class: "down-icon",
    type: "icon-down",
    dropOne: "Todos os marketplaces",
    dropTwo: "produtos",
    value: "6.587",
  },
];

const columns = columnsTable.columnsTable.map((temp) => ({
  ...temp,
  render: (text, record) => <div>{text}</div>,
}));

export default function pedido() {
  const [visibles, setVisibles] = React.useState(false);
  const [className, setClassName] = React.useState("tamanho_100");
  const [clasActive, setClasActive] = React.useState(false);
  const [none, setNone] = React.useState("d-none");
  const [pedidos, setPedido] = React.useState();
  const [data, setData] = React.useState([]);
  useEffect(async () => {
    let data = await TableData();
    data = dataTable.dataTable.map((temp) => ({
      ...temp,
      name: (
        <>
          {temp.name.length > 0 &&
            temp.name.map((tempName) => {
              return <p>{tempName}</p>;
            })}
        </>
      ),
      action:
        temp.action &&
        temp.action.map((tempAction) => {
          return (
            <div className="d-flex align-items-center align">
              {tempAction.icons.map((tempIcons) => {
                return (
                  <div className="iconMargin">
                    {Icon(tempIcons, "", {
                      width: "24px",
                      heigth: "24px",
                    })}
                  </div>
                );
              })}
    
              {tempAction.icon.map((tempIcon) => {
                return (
                  <a href={tempAction.to} className={tempAction.className}>
                    <div className={tempAction.className}>
                      {Icon(tempIcon, "", { width: "24px", heigth: "24px" })}
                    </div>
                  </a>
                );
              })}
            </div>
          );
        }),
      status: (
        <div>
          <Tooltip placement="bottom" title="Pedido cancelado">
            <span className="dot mv-auto"></span>
          </Tooltip>
        </div>
      ),
      img: (
        <div>
          <img src={temp.img} alt="Imagem do produto" className="imgs" />
        </div>
      ),
      estoque: <div>{temp.estoque}</div>,
    }));
    setData(data);
  }, []);
  function visbles(pedido, index) {
    setVisibles(!visibles);

    [...document.getElementsByClassName(`active`)].map((active) =>
      active.classList.remove("active")
    );
    setTimeout(() => {
      if (!visibles || pedido != pedidos) {
        setNone("d-block");
        setClassName("tamanho_70");
        setVisibles(true);
        [...document.getElementsByClassName(`row_${index}`)][0].classList.add(
          "active"
        );
        setClasActive(index);
      } else {
        setNone("d-none");
        setClassName("tamanho_100");
        setClasActive(false);
      }
      setPedido(pedido);
    }, 200);
  }

  function onClose() {
    setVisibles(false);
    setTimeout(() => {
      setNone("d-none");
      setClassName("tamanho_100");
    }, 200);
  }
  return (
    <Styles>
      <div className={className}>
        <Filtro campos={jsonDefault} />
        <Table
          dataTable={data}
          columnsTable={columns}
          headerTable={headerTable}
          visbles={visbles}
          className={clasActive}
          selectPedidos={pedidos}
        />
      </div>
      <div className={`site-drawer-render-in-current-wrapper ${none}`}>
        <Drawer vesibles={visibles} onClose={onClose} drawerJson={drawerJson} />
      </div>
    </Styles>
  );
}
