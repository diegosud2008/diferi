import React from "react";
import Details from "../../global/Details";
import Filtro from "../../filtros/Filtro";
import { Icon } from "../../../controller/icon";
import Styles from "../../../styles/components/publicacoes/detalhes";

const dataTable = require("../../../variables/publicacoes/detalhes/dataTable.json");
const columnsTable = require("../../../variables/publicacoes/detalhes/columnsTable.json");
const headerTable = false;

const columns = columnsTable.columnsTable.map((temp) => ({
  ...temp,
}));

const data = dataTable.dataTable.map((temp) => ({
  ...temp,
  status: <div className={temp.className}>{temp.status}</div>,
  icon: Icon(temp.icon),
}));

const propsJson = require("../../../variables/publicacoes/detalhes/propsJson.json");
const infosArray = require("../../../variables/publicacoes/detalhes/details.json");
const jsonDefault = [
  {
    type: "back",
    to: "/hubba/publicacoes",
    icon: "arrowLeft",
    className: "back-icon",
  },
  {
    type: "divider",
    direction: "vertical",
    className: "divider-style",
  },
  {
    type: "points",
  },
];

export default function detalhes() {
  return (
    <Styles>
      <Filtro campos={jsonDefault} />
      <Details
        infosArray={infosArray}
        headerTable={headerTable}
        propsJson={propsJson}
        dataTable={data}
        columnsTable={columns}
      />
    </Styles>
  );
}
