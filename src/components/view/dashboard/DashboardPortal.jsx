import React from 'react';
import Styles from '../../../styles/components/dashboards/portal'
import LineChartDashboard from "../../dashboards/charts/LineChartDashboard"
import CardsChartDashboard from "../../dashboards/charts/CardsChartDashboard"
import VerticalHeaderTable from "../../dashboards/VerticalHeaderTable"
import HighlightProduct from "../../dashboards/HighlightProduct"

const portalData = require("../../../variables/dashboards/dashboardPortal.json");

const jsonConfig = {
    row1: [
        {
            name: "cardsChart",
            key: '1',
            Component: <CardsChartDashboard data={portalData.cardsChart.data} />,
        }
    ],
    row2: [
        {
            name: "tableChart1",
            key: '2',
            Component: <VerticalHeaderTable data={portalData.tableChart.table1} />,
        },
        {
            name: "highlightChart1",
            key: '3',
            Component: <HighlightProduct data={portalData.highlightChart.highlight1} />,
        },
        {
            name: "highlightChart2",
            key: '4',
            Component: <HighlightProduct data={portalData.highlightChart.highlight2} />,
        },
        {
            name: "tableChart2",
            key: '5',
            Component: <VerticalHeaderTable data={portalData.tableChart.table2} />,
        }
    ],
    row3: [
        {
            name: "lineChart",
            key: '6',
            Component: <LineChartDashboard data={portalData.lineChart} />,
        }
    ]
}

function dashboardPortal() {
    return (
        <Styles>
            <div className="mainPortal">
                {Object.keys(jsonConfig).map(row => {
                    {
                        return (
                            <div className="row">
                                {jsonConfig[row].map(el => (
                                    <div className={el.name}>{el.Component}</div>
                                ))}
                            </div>
                        )
                    }
                })}
            </div>
        </Styles>
    );
}
export default dashboardPortal;