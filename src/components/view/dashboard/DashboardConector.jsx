import React from 'react';
import Styles from '../../../styles/components/dashboards/conector'
import DoublePieChartWithInnerLabelDashboard from "../../dashboards/charts/DoublePieChartWithInnerLabel/DoublePieChartWithInnerLabelDashboard"
import LineChartDashboard from "../../dashboards/charts/LineChartDashboard"
import CardsChartDashboard from "../../dashboards/charts/CardsChartDashboard"

const conectorData = require("../../../variables/dashboards/dashboardConector.json");

const jsonConfig = [
    {
        name:"cardsChart",
        key:'1',
        Component: <CardsChartDashboard data={conectorData.cardsChart.data}/>,
    },
    {
        name:"pieChart",
        key:'2',
        Component: <DoublePieChartWithInnerLabelDashboard data1={conectorData.pieChart.data1} data2={conectorData.pieChart.data2} />,
    },
    {
        name:"lineChart",
        key:'3',
        Component: <LineChartDashboard data={conectorData.lineChart} />,
    }
]

function dashboardConector() {
    return (
        <Styles>
            <div className="mainConector">
                {jsonConfig.map((el) => {
                    return(
                        <div className={el.name}>{el.Component}</div>
                    )
                })}
            </div>
        </Styles>
    );
}
export default dashboardConector;