import React, { useState, useRef, Component } from "react"
import StyleLogin from '../../../styles/components/login/login'
import LogoIta from '../../../assets/itadigital.png'
import Link from "next/dist/client/link"

export function Login() {

    const [spinner, setSpinner] = useState('hide');
    
    function changeSpinner() {
        setSpinner('spinner');
    }

        return (
            <StyleLogin>
                <div className="Login">
                    <div className="main">
                        <h1>HUBBA</h1>
                        <p>Sistema integrado para marketplaces</p>
                    </div>
                    <div className="formulario">
                        <h3>Entre na sua conta</h3>
                        <small>Usuário</small><br />
                        <input type="text" className="box" id="user" /><br />
                        <small>Senha</small><br />
                        <input type="password" className="box" id="password" /><br />
                        <small className="recuperar-senha"><a href="#">Esqueci minha senha</a></small><br />
                        <Link href="pedidos">
                            <button type="submit" className="button" onClick={ changeSpinner }>
                                Entrar
                            </button>
                        </Link>
                        <div className={ spinner }/>
                    </div>
                    <img src={LogoIta.src} alt="" />
                </div>
            </StyleLogin >

        )
    }