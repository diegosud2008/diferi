import React, { useEffect } from "react";
import Filtro from "../../filtros/Filtro";
import Table from "../../global/Table";
import Drawer from "../../pedidos/Drawer";
import Styles from "../../../styles/components/imoveis/default";
import { TableData } from "../../../controller/pages/imoveis";
import { Icon } from "../../../controller/icon";
import { mask } from "../../../controller/functions";

const drawerJson = require("../../../variables/imoveis/drawer/drawerJson.json");

const columnsTable = require("../../../variables/imoveis/columnsTable.json");
const headerTable = false;

import { Tooltip } from "antd";

const jsonDefault = [
  {
    divider: false,
  },
  {
    icon: "PlaneOutline",
    type: "plane",
    className: "planeIcon",
    dividerSrc: true,
    dividerDirection: "vertical"
  },
  // {
  //   type: "divider",
  //   direction: "vertical",
  //   className: "divider-style",
  // },
  {
    icon: "FilterOutlined",
    className: "filterIconProd",
    type: "filter",
  },
  {
    icon: "SearchOutlined",
    className: "srcIconProd",
    type: "none",
  },
  {
    input: "Search",
    dividerSrc: true,
    escSrc:true,
    classNameEsc:"escIcon",
    dividerDirection: "vertical",
    className: "srcIconPed",
    type: "input",
  },
  {
    icon: "MoreOutlined",
    className: "list-icon",
    type: "points",
  },
  {
    class: "down-icon",
    type: "icon-down",
    dropOne: "Todos os marketplaces",
    dropTwo: "produtos",
    value: "18.054",
  },
];

const columns = columnsTable.columnsTable.map((temp) => ({
  ...temp,
  render: (text, record) => <div>{text}</div>,
}));

export default function pedido() {
  const [visibles, setVisibles] = React.useState(false);
  const [className, setClassName] = React.useState("tamanho_100");
  const [clasActive, setClasActive] = React.useState(false);
  const [none, setNone] = React.useState("d-none");
  const [pedidos, setPedido] = React.useState();
  const [data, setData] = React.useState([]);

  useEffect(async () => {
    let data = await TableData();
    data = data.dataTable.map((temp) => ({
      ...temp,
      name: (
        <div className="dados">
          {
            temp.name.map( dados => {
              return (
                <>
                  <strong>{dados.title}</strong>
                  <div className="bloco">
                    <span className="tipo">{dados.tipo}</span>
                    <span>visitas: {dados.visitas}</span>
                    <span>Propostas: {dados.propostas}</span>
                    {
                      dados.propietario.map( tempP => {
                        return(
                          <span className="dados-proprietario">
                            {tempP && <span>{tempP.nome}</span>}
                            {tempP.email && <span>{tempP.email}</span>}
                          </span>
                        )
                      })
                    }
                  </div>
                  <div className="bloco">
                    <span>Tipo: {dados.venda && dados.aluguel ? `Venda/Aluguel` : dados.venda ? `Venda` : dados.aluguel ? `Aluguel` : ''}</span>
                    { dados.preco_venda > 0 && <span>Preço venda: { mask(dados.preco_venda)}</span> }
                    { dados.preco_locacao > 0 && <span>Preço locação: { mask(dados.preco_locacao)}</span> }
                  </div>
                  <div className="bloco">
                    <span>Estado: {dados.estado}</span>
                    <span>Cidade: {dados.cidade}</span>
                    <span>Bairro: {dados.bairro}</span>
                    <span>Endereço: {dados.endereco}</span>
                    <span>Número: {dados.numero}</span>
                  </div>
                </>
              )
            })
          }
        </div>
      ),
      imagem: (
        <div className="imagem">
          <img src={temp.imagem} />
        </div>
      ),
      status: (
        <div>
          <Tooltip placement="bottom" title="Imóvel cancelado">
            <span className="dot mv-auto "></span>
          </Tooltip>
        </div>
      ),
      icon: Icon(temp.icon),
      action: temp.action.map((tempAction) => {
        return (
          <div className="d-flex align-items-center align justify-content-between">
            <strong>{tempAction.price}</strong>
            <a
              href={`${tempAction.to}/${temp.id}`}
              className={tempAction.className}
            >
              {Icon(tempAction.icon, "", { width: "24px", heigth: "24px" })}
            </a>
          </div>
        );
      }),
    }));
    setData(data);
  }, []);
  function visbles(pedido, index) {
    setVisibles(!visibles);

    [...document.getElementsByClassName(`active`)].map((active) =>
      active.classList.remove("active")
    );
    setTimeout(() => {
      if (!visibles || pedido != pedidos) {
        setNone("d-block");
        setClassName("tamanho_70");
        setVisibles(true);
        [...document.getElementsByClassName(`row_${index}`)][0].classList.add(
          "active"
        );
        setClasActive(index);
      } else {
        setNone("d-none");
        setClassName("tamanho_100");
        setClasActive(false);
      }
      setPedido(pedido);
    }, 200);
  }

  function onClose() {
    setVisibles(false);
    setTimeout(() => {
      setNone("d-none");
      setClassName("tamanho_100");
    }, 200);
  }
  return (
    <Styles>
      <div className={className}>
        <Filtro campos={jsonDefault}/>
        <Table
          dataTable={data}
          columnsTable={columns}
          headerTable={headerTable}
          visbles={visbles}
          selectPedidos={pedidos}
        />
      </div>
      <div className={`site-drawer-render-in-current-wrapper ${none}`}>
        <Drawer vesibles={visibles} onClose={onClose} drawerJson={drawerJson} />
      </div>
    </Styles>
  );
}
