import React from "react";
import Link from "next/link";
import Details from "../../global/Details";
import Table from "../../global/Table";
import TableCss from "../../../styles/components/imoveis/detalhes/tableDetails";
import Filter from "../../filtros/Filtro";
import { Tooltip } from "antd";
import { Icon } from "../../../controller/icon";

const infosArray = require("../../../variables/imoveis/detalhes/details.json");
const dataTable = require("../../../variables/imoveis/detalhes/dataTable.json");
const columnsTable = require("../../../variables/imoveis/detalhes/columnsTable.json");
const headerTable = false;
const columns = columnsTable.columnsTable.map((temp) => ({
  ...temp,
  render: (text, record) => <div>{text}</div>,
}));

const data = dataTable.dataTable.map((temp) => ({
  ...temp,
  action:
    temp.action &&
    temp.action.map((tempAction) => {
      return (
        <div className="d-flex align-items-center align">
          {tempAction.icons.map((tempIcons) => {
            return (
              <div className="iconMargin">
                {Icon(tempIcons, "", {
                  width: "24px",
                  heigth: "24px",
                })}
              </div>
            );
          })}

          {tempAction.icon.map((tempIcon) => {
            return (
              <Link href={tempAction.to} className={tempAction.className}>
                <div className={tempAction.className}>
                  {Icon(tempIcon, "", { width: "24px", heigth: "24px" })}
                </div>
              </Link>
            );
          })}
        </div>
      );
    }),
  status: (
    <Tooltip placement="bottom" title="Pedido cancelado">
      <span className="dot mv-auto"></span>
    </Tooltip>
  ),
  img: <img src={temp.img} alt="Imagem do produto" className="imgs" />,
  estoque: <div className={temp.className}>{temp.estoque}</div>,
}));

const propsJson = require("../../../variables/imoveis/detalhes/propsJson.json");

const jsonDefault = [
  {
    type: "back",
    to: "/hubba/produtos",
    icon:"arrowLeft",
    className: "back-icon"
  },
  {
    type: "divider", 
    direction: "vertical",
    className: "divider-style",
  },
  {
    type: "points",
    className: "list-icon-produtos"
  },
  {
    type: "tabs",
    menus: [
      {
        disabled: false,
        key: "1",
        label: "PROPRIEDADES",
        Component: <Details propsJson={propsJson} infosArray={infosArray} />,
      },
      {
        disabled: false,
        key: "2",
        label: "PUBLICAÇÕES",
        Component: (
          <TableCss>
            <Table
              dataTable={data}
              columnsTable={columns}
              headerTable={headerTable}
            />
          </TableCss>
        ),
      },
      {
        disabled: true,
        key: "3",
        label: "HISTÓRICO",
        Component: <Details />,
      },
    ],
  },
];

export default class detalhes extends React.Component {
  state = {
    local: "PROPRIEDADES",
  };

  changeTabs = (locations) => {
    this.setState({ local: locations });
  };

  change = () => {
    console.log("this.state.local");
    return <Details propsJson={propsJson} infosArray={infosArray} />;
  };

  render() {
    return (
      <div>
        {/* <Filter campos={jsonDefault} Modified="hr_none"/> */}
        <Filter
          campos={jsonDefault}
          changeTabs={this.changeTabs}
          Modified="ml-2 dotsDetails detailsFilter hr_none"
        />
        {jsonDefault[3].menus.map((temp) => {
          if (temp.label == this.state.local) {
            return temp.Component;
          }
        })}
      </div>
    );
  }
}
