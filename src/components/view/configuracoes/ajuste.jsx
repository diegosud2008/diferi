import React from "react";
import Filter from '../../../components/filtros/Filtro'
import Details from '../../global/DetailsConfigs'
import TableCss from '../../../styles/components/produtos/detalhes/tableDetails'
import ConfigCss from '../../../styles/components/configuracoes/configuracoes'
import { Table, Tooltip } from "antd";
import { Icon } from '../../../controller/icon'
import Link from "next/dist/client/link";

const propsDetails = require("../../../variables/configuracoes/propsDetails.json");

const infosPreco = require("../../../variables/configuracoes/infosPreco.json")
const infosAutentic = require("../../../variables/configuracoes/infosAut")
const infoasFrete = require("../../../variables/configuracoes/infosFrete.json")
const infosAnuncio = require("../../../variables/configuracoes/infosAnuncio.json")
const infosVendas = require("../../../variables/configuracoes/infosVendas.json")

const dataTable = require("../../../variables/produtos/detalhes/dataTable.json");
const columnsTable = require("../../../variables/produtos/detalhes/columnsTable.json");
const headerTable = false;
const columns = columnsTable.columnsTable.map((temp) => ({
    ...temp,
    render: (text, record) => <div>{text}</div>,
}));


const data = dataTable.dataTable.map((temp) => ({
    ...temp,
    action:
        temp.action &&
        temp.action.map((tempAction) => {
            return (
                <div className="d-flex align-items-center align">
                    {tempAction.icons.map((tempIcons) => {
                        return (
                            <div className="iconMargin">
                                {Icon(tempIcons, "", {
                                    width: "24px",
                                    heigth: "24px",
                                })}
                            </div>
                        );
                    })}
                    {tempAction.icon.map((tempIcon) => {
                        return (
                            <Link href={tempAction.to} className={tempAction.className}>
                                <div className={tempAction.className}>
                                    {Icon(tempIcon, "", { width: "24px", heigth: "24px" })}
                                </div>
                            </Link>
                        );
                    })}
                </div>
            );
        }),
    status: (
        <Tooltip placement="bottom" title="Pedido cancelado">
            <span className="dot mv-auto"></span>
        </Tooltip>
    ),
    img: <img src={temp.img} alt="Imagem do produto" className="imgs" />,
    estoque: <div className={temp.className}>{temp.estoque}</div>,
}));


const jsonDefault = [
    {
        type: "back",
        to: "/hubba/configuracoes/configs",
        icon: "arrowLeft",
        className: "back-icon"
    },
    {
        type: "tabs",
        menus: [
            {
                disabled: false,
                key: "1",
                label: "AUTENTICAÇÃO",
                Component: <Details infosArray={infosAutentic} propsDetails={propsDetails} />,
            },
            {
                disabled: false,
                key: "2",
                label: "CONFIGURAÇÃO DE PREÇO",
                Component: <Details infosArray={infosPreco} propsDetails={propsDetails} />,
            },
            {
                disabled: false,
                key: "3",
                label: "CONFIGURAÇÃO DE FRETE",
                Component: <Details infosArray={infoasFrete} propsDetails={propsDetails} />,
            },
            {
                disabled: false,
                key: "4",
                label: "CONFIGURAÇÃO DE ANUNCIOS",
                Component: <Details infosArray={infosAnuncio} propsDetails={propsDetails} />,
            },
            {
                disabled: false,
                key: "5",
                label: "CONFIGURAÇÃO DE VENDAS",
                Component: <Details infosArray={infosVendas} propsDetails={propsDetails} />,
            },
        ],
    },
];

export class AjusteConfig extends React.Component {
    state = {
        local: "AUTENTICAÇÃO",
    };

    changeTabs = (locations) => {
        this.setState({ local: locations });
    };

    change = () => {
        console.log("this.state.local");
        return <Details propsJson={propsJson} infosArray={infosArray} />;
    };

    render() {
        return (
            <ConfigCss>
                <div className="container">
                    <Filter campos={jsonDefault} changeTabs={this.changeTabs}/>
                    {jsonDefault[1].menus.map((temp) => {
                        if (temp.label == this.state.local) {
                            return temp.Component;
                        }
                    })}
                </div>
            </ConfigCss>
        )
    }
}