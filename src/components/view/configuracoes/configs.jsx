import React, { useState, useEffect } from "react";
import Table from '../../global/TableManutencao'
import Filter from '../../filtros/Filtro'
import { Icon } from '../../../controller/icon'
import StyleConfig from '../../../styles/components/configuracoes/configuracoes'
import Link from "next/dist/client/link";

const dados = require("../../../variables/configuracoes/data_table.json")
const coluns = require("../../../variables/configuracoes/columns_table.json")
const setHeader = false;

function visbles() {}

const jsonDefault = [
    {
        icon: "SearchOutlined",
        className: "srcIconProd",
        type: "none",
    },
    {
        input: "Search",
        dividerSrc: false,
        dividerDirection: "vertical",
        className: "srcIconPed",
        type: "input",
    }
];

export function Configs() {
    const [value, setValue] = useState(dados)
    const [btnConfig, setBtnConfig] = useState('disabled');
    const [data, setData] = useState([])

    useEffect( async () => {
        const data = dados.dataTable.map((temp) => ({
            ...temp,
            icon: (
                Icon(temp.icon)
            ),
            button: (
                <button onClick={() => changeValue(temp.key)} className="buttonActive" >{temp.valueButton}</button>
            ),
            button2: (
                <Link href="/hubba/configuracoes/ajuste"><button className={temp.statusBtn}>{temp.valueConfig}</button></Link>
            ),
        }));
        setData(data);
    })

    function changeValue(kay) {
        dados.dataTable[kay - 1].valueButton == "ATIVAR" ? setValue(dados.dataTable[kay - 1].valueButton = "DESATIVAR") : setValue(dados.dataTable[kay - 1].valueButton = "ATIVAR")
        dados.dataTable[kay - 1].valueButton == "ATIVAR" ? setBtnConfig(dados.dataTable[kay - 1].statusBtn = 'disabled') : setBtnConfig(dados.dataTable[kay - 1].statusBtn = 'btnEnabled');

        console.log(dados.dataTable[kay - 1].statusBtn);
    }

    return (
        <StyleConfig>
            <div className="container">
                <Filter campos={jsonDefault} />
                <Table header_table={setHeader} columns_table={coluns.columnsTable} data_table={data} visbles={visbles}/>
            </div>
        </StyleConfig>
    )
}