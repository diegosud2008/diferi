import React from "react";
import Details from "../../global/Details";
import Filter from "../../filtros/Filtro";

/**Dados tab 1 */
const infosArray = require("../../../variables/pedidos/detalhes/details.json");
const propsJson = require("../../../variables/pedidos/detalhes/propsJson.json");

/**Dados tab 2 */
const infosArray2 = require("../../../variables/pedidos/detalhes/details2.json");

/**Dados tab 3 */
const infosArray3 = require("../../../variables/pedidos/detalhes/details3.json");

const jsonDefault = [
  {
    type: "back",
    icon: "arrowLeft",
    to: "/hubba/manutencao/manutencao",
    className: "back-icon"
  },
  {
    type: "divider",
    direction: "vertical",
    className: "divider-style",
  },
  {
    type: "points",
    icon: "dotsVertical",
    className: "dotsVertical"
  },
  {
    type: "tabs",
    menus: [
        {
            key: "1",
            label: "DETALHES",
            Component: <Details infosArray={infosArray3} propsJson={propsJson} />,
        },
        {
            key: "2",
            label: "PRODUTOS",
            Component: <Details infosArray={infosArray2} propsJson={propsJson} />,
        },
        {
          key: "3",
          label: "PROPRIEDADES",
          Component: <Details infosArray={infosArray} propsJson={propsJson} />,
        },
    ],
  },
];

export class Detalhes extends React.Component {
    state = {
    local: "DETALHES",
};

  changeTabs = (locations) => {
    this.setState({ local: locations });
  };

  change = () => {
    console.log("this.state.local");
    return (
      <Details infoJson={infoJson} clientJson={clientJson} payJson={payJson} />
    );
  };
  render() {
    return (
      <div>
        <Filter campos={jsonDefault} changeTabs={this.changeTabs} />
        {jsonDefault[3].menus.map((temp) => {
          if (temp.label == this.state.local) {
            return temp.Component;
          }
        })}
      </div>
    );
  }
}
