import React, { Component, useState } from "react";
import { Icon } from "../../../controller/icon";
import Link from "next/dist/client/link";
import StyleManutencao from "../../../styles/components/manutencao/prazo";
import Table from "../../global/TableManutencao";
import Filter from '../../filtros/Filtro'

const dados = require("../../../variables/pedidos/table_manutencao/data_table.json"); //DADOS GERAIS DA TABELA MANUTENÇÃO
const coluns = require("../../../variables/pedidos/table_manutencao/columns_table.json"); //COLUNAS GERAIS DA TABELA MANUTENÇÃO
const marketplaces = require("../../../variables/pedidos/table_manutencao/marketplaces.json"); //LISTA DOS MARKETPLACES
const statusMkp = require("../../../variables/pedidos/table_manutencao/lista_status.json"); // LISTA DOS IDS DOS MARKETPLACES


//LISTA DOS MARKETPLACES
const listMarket = marketplaces.marketplacesJson.map((items) => {
  return <option value="marketplaces">{items.label}</option>;
});
//LISTA DOS CÓDIGOS DOS MARKETPLACES
const listCodMarket = marketplaces.marketplacesJson.map((items) => {
  return <option value="marketplaces">{items.cod_marketplace}</option>;
});
//LISTA DOS STATUS DE PEDIDOS
const listStatus = statusMkp.status_pedidoJson.map((items) => {
  return <option value="marketplaces">{items.label}</option>;
});
//RENDERIZA O ICONE EDIT NA TABLE
const data = dados.dataTable.map((temp) => ({
  ...temp,
  icon: (
    <Link href="/hubba/manutencao/detalhes">
      <div>{Icon(temp.icon)}</div>
    </Link>
  ),
}));


//RELAÇÃO DE ICONES ESPECIFICOS DA PAGINA
const jsonDefault = [
  {
    icon: "",
    className: "campo-date",
    type: "field-date"
  },
  {
    icon: "FilterOutlined",
    className: "filterIconPed",
    type: "filter"
  },
  {
    icon: "SearchOutlined",
    className: "srcIconProd",
    type: "input"
  }
];
export class Manutencao extends Component {
  render() {
    return (

      <StyleManutencao>
        <div className="container">
          <Filter campos={jsonDefault} />
          <div>
            <Table columns_table={coluns.columnsTable} data_table={data} />
          </div>
        </div>
      </StyleManutencao>
    );
  }
}
