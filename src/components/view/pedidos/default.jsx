import React, { useEffect } from "react";
import Filtro from "../../filtros/Filtro";
import Table from "../../global/Table";
import Drawer from "../../pedidos/Drawer";
import Styles from "../../../styles/components/pedidos/default";
import { Icon } from "../../../controller/icon";
import { TableData } from "../../../controller/pages/pedidos";

const drawerJson = require("../../../variables/pedidos/drawer/drawerJson.json");
const dataTable = require("../../../variables/pedidos/dataTable.json");
const columnsTable = require("../../../variables/pedidos/columnsTable.json");
const headerTable = false;

import {
  CaretDownFilled,
} from "@ant-design/icons";
import { Input, Tooltip } from "antd";

const { Search } = Input;

const jsonDefault = [
  {
    divider: false,
  },
  {
    icon: "FilterOutlined",
    className: "filterIconPed",
    type: "filter",
  },
  {
    icon: "SearchOutlined",
    className: "srcIconProd",
    type: "none",
  },
  {
    input: "Search",
    dividerSrc: true,
    dividerDirection: "vertical",
    className: "srcIconPed",
    type: "input",
  },
  {
    icon: "MoreOutlined",
    class: "list-icon",
    type: "points",
  },
  {
    icon: CaretDownFilled,
    class: "down-icon",
    type: "icon-down",
    dropOne: "Todos os marketplaces",
    dropTwo: "pedidos",
    value: "6.587",
    onDrawer: false
  },
  {
    type: "dividerTextFilter",
    direction: "vertical",
    className: "divider-text",
    onDrawer: false
  },
  {
    icon: CaretDownFilled,
    class: "down-icon",
    type: "icon-down-mkt",
    dropOne: "Todos os marketplaces",
    dropTwo: "pedidos",
    value: "6.587",
    onDrawer: false
  },
  {
    class: "down-icon",
    type: "icon-down",
    dropOne: "Todos os marketplaces",
    dropTwo: "pedidos",
    value: "6.587",
    onDrawer: true
  },
];

const columns = columnsTable.columnsTable.map((temp) => ({
  ...temp,
  render: (text, record) => <div>{text}</div>,
}));

export default function pedido() {
  const [visibles, setVisibles] = React.useState(false);
  const [className, setClassName] = React.useState("tamanho_100");
  const [clasActive, setClasActive] = React.useState(false);
  const [none, setNone] = React.useState("d-none");
  const [pedidos, setPedido] = React.useState();
  const [data, setData] = React.useState([]);
  useEffect(async () => {
    let data = await TableData();
    data = dataTable.dataTable.map((temp) => ({
      ...temp,
      status: (
        <div>
          <Tooltip placement="bottom" title="Pedido cancelado">
            <span className="dot mv-auto "></span>
          </Tooltip>
        </div>
      ),
      icon: Icon(temp.icon),
      action: temp.action.map((tempAction) => {
        console.log(data,pedidos,none,clasActive,className)
        return (
          <div className="d-flex align-items-center align justify-content-between">
            <strong>{tempAction.price}</strong>
            <a
              href={`${tempAction.to}/${temp.id}`}
              className={tempAction.className}
            >
              {Icon(tempAction.icon, "", { width: "24px", heigth: "24px" })}
            </a>
          </div>
        );
      }),
    }));
    setData(data);
  }, []);
  function visbles(pedido, index) {
    setVisibles(!visibles);
    console.log(data,pedidos,none,clasActive,className);
    [...document.getElementsByClassName(`active`)].map((active) =>
      active.classList.remove("active")
    );
    setTimeout(() => {
      if (!visibles || pedido != pedidos) {
        setNone("d-block");
        setClassName("tamanho_70");
        setVisibles(true);
        [...document.getElementsByClassName(`row_${index}`)][0].classList.add(
          "active"
        );
        setClasActive(index);
      } else {
        setNone("d-none");
        setClassName("tamanho_100");
        setClasActive(false);
      }
      setPedido(pedido);
    }, 200);
  }

  function onClose() {
    setVisibles(false);
    setTimeout(() => {
      setNone("d-none");
      setClassName("tamanho_100");
    }, 200);
  }
  return (
    <Styles>
      <div className={className}>
        <Filtro drawerStatus={visibles} campos={jsonDefault} />
        <Table
          dataTable={data}
          columnsTable={columns}
          headerTable={headerTable}
          visbles={visbles}
          className={clasActive}
          selectPedidos={pedidos}
        />
      </div>
      <div className={`site-drawer-render-in-current-wrapper ${none}`}>
        <Drawer vesibles={visibles} onClose={onClose} drawerJson={drawerJson} />
      </div>
    </Styles>
  );
}
