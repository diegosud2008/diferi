import React from "react";
import Details from "../../global/Details";
import Filter from "../../filtros/Filtro";
// const infoJson = require("../../../variables/pedidos/detalhes/infoJson.json");
// const clientJson = require("../../../variables/pedidos/detalhes/clientJson.json");
// const payJson = require("../../../variables/pedidos/detalhes/payJson.json");
// const nfJson = require("../../../variables/pedidos/detalhes/nfJson.json");
// const productJson1 = require("../../../variables/pedidos/detalhes/productJson1.json");
// const productJson2 = require("../../../variables/pedidos/detalhes/productJson2.json");
// const productJson3 = require("../../../variables/pedidos/detalhes/productJson3.json");
// const deliveryJson = require("../../../variables/pedidos/detalhes/deliveryJson.json");
// const commentsJson = require("../../../variables/pedidos/detalhes/commentsJson.json");

/**Dados tab 1 */
const infosArray = require("../../../variables/pedidos/detalhes/details.json");
const propsJson = require("../../../variables/pedidos/detalhes/propsJson.json");

/**Dados tab 2 */
const infosArray2 = require("../../../variables/pedidos/detalhes/details2.json");

/**Dados tab 3 */
const infosArray3 = require("../../../variables/pedidos/detalhes/details3.json");

const jsonDefault = [
  {
    type: "back",
    icon: "arrowLeft",
    to: "/hubba/pedidos",
    className: "back-icon"
  },
  {
    type: "divider",
    direction: "vertical",
    className: "divider-style",
  },
  {
    type: "points",
    icon: "dotsVertical",
    className: "dotsVertical"
  },
  {
    type: "tabs",
    menus: [
      {
        key: "1",
        label: "PROPRIEDADES",
        Component: <Details infosArray={infosArray} propsJson={propsJson} />,
      },
      {
        key: "2",
        label: "PRODUTOS",
        Component: <Details infosArray={infosArray2} propsJson={propsJson} />,
      },
      {
        key: "3",
        label: "DETALHES",
        Component: <Details infosArray={infosArray3} propsJson={propsJson} />,
      },
    ],
  },
];

export default class detalhes extends React.Component {
  state = {
    local: "PROPRIEDADES",
  };

  changeTabs = (locations) => {
    this.setState({ local: locations });
  };

  change = () => {
    console.log("this.state.local");
    return (
      <Details infoJson={infoJson} clientJson={clientJson} payJson={payJson} />
    );
  };
  render() {
    return (
      <div>
        <Filter campos={jsonDefault} changeTabs={this.changeTabs} />
        {jsonDefault[3].menus.map((temp) => {
          if (temp.label == this.state.local) {
            return temp.Component;
          }
        })}
      </div>
    );
  }
}
