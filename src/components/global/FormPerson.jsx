import React, { useState } from "react";
import { Input, Select, Checkbox, Form, Cascader, Typography, Button, Switch, DatePicker } from "antd";
import { Icon } from "../../controller/icon";
import Style from "../../styles/components/global/form";
import moment from 'moment';

const { Option } = Select;

const { TextArea } = Input;
const { Text, Link } = Typography;
const dateFormat = 'DD/MM/YYYY';

function handleChange(value) {
  console.log(`selected ${value}`);
}

const children = [];
for (let i = 10; i < 36; i++) {
  children.push(<Option key={i.toString(36) + i}>{i.toString(36) + i}</Option>);
}
/**
 * @param {config} Parâmetro que recebe um array para montar os elementos
 * @return <componente a ser renderizado/>
 **/
function input({ config }) {
  const [input2, setInput] = useState(true);
  return (
    <Style {...config}>
      {config.type === "input" && (
        <div className={config.classNameDiv}>
          <Form layout="vertical contenct">
            <Form.Item name={config.name}
              label={
                <div className="label-form">
                  {config.label}
                  {config.icon && (
                    <span className="d-flex">
                      {Icon(config.icon, "", {
                        width: "13.31px",
                        heigth: "13.31px",
                      })}
                    </span>
                  )}
                </div>
              }
            >
              <Input
                className={config.className}
                defaultValue={config.value}
                disabled={config.desable}
                placeholder={config.placeholder}
              />
              <div className="link-style">
                {Icon(config.link, "", {
                  width: "13.31px",
                  heigth: "13.31px",
                })}
              </div>
            </Form.Item>
          </Form>
        </div>
      )}
      {config.type === "text-area" && (
        <Form layout="vertical">
          <Form.Item name={config.name} label={config.label}>
            <div className={config.classNameDiv}>
              <TextArea
                className={config.className}
                defaultValue={config.value}
                disabled={config.desable}
                rows={config.rows}
              />
            </div>
          </Form.Item>
        </Form>
      )}
      {config.type === "select" && (
        <Form layout="vertical">
          <Form.Item label={config.label} name={config.name}>
            <div className={config.classNameDiv}>
              <Select
                allowClear={config.allowClear}
                size="large"
                disabled={config.desable}
                defaultValue={config.defaultValue}
                className={config.className}
                onChange={handleChange}
                placeholder={config.placeholder}
                mode={config.mode}
              >
                {config.options.map((temp) => {
                  return (
                    <Option disabled={temp.disabled} value={temp.value}>
                      {temp.text}
                    </Option>
                  );
                })}
              </Select>
              <div className="link-style">
                {Icon(config.link, "", {
                  width: "13.31px",
                  heigth: "13.31px",
                })}
              </div>
            </div>
          </Form.Item>
        </Form>
      )}

      {config.type === "checkbox" && (
        <Form layout="vertical">
          <Form.Item name={config.name}>
            <div className={config.classNameDiv}>
              <Checkbox name={config.name} className={config.className}>
                {config.label && (
                  <div className="d-flex">
                    <spam>{config.label}</spam>
                    {Icon(config.icon, "", {
                      width: "13.31px",
                      heigth: "13.31px",
                    })}
                  </div>
                )}
                {config.value}
              </Checkbox>
            </div>
          </Form.Item>
        </Form>
      )}
      {config.type === "cascate" && (
        <Form layout="vertical">
          <Form.Item label={config.label} name={config.name}>
            <div className={config.classNameDiv}>
              <Cascader
                className={config.className}
                defaultValue={["zhejiang", "hangzhou", "xihu"]}
                disabled={config.desable}
                size="large"
                options={config.options}
                placeholder="Please select"
                mode={config.mode}
                allowClear={config.allowClear}
              />
            </div>
          </Form.Item>
        </Form>
      )}
      {config.type === "text" && (
        <Form layout="vertical">
          <Form.Item label={config.label} name={config.name}>
            <Text
              type={config.textStyle}
              disabled={config.disabled}
              strong={config.strong}
              className={config.className}
            >
              {config.value}
            </Text>
          </Form.Item>
        </Form>
      )}
      {config.type === "button" && (
        <Form layout="vertical">
          <Form.Item label={config.label} name={config.name}>
            <Button
              type={config.buttonStyle}
              disabled={config.disabled}
              className={config.className}
            >
              {config.value}
            </Button>
          </Form.Item>
        </Form>
      )}
      {config.type === "switch" && (
        <Form layout="vertical">
          <Form.Item label={config.label} name={config.name}>
            <Switch
              checked={config.checked}
              name={config.name}
              checkedChildren={config.value1}
              unCheckedChildren={config.value2}
              className={config.className}
              onChange={() => {
                setInput(!input2);
              }}
            />
          </Form.Item>
        </Form>
      )}
      {config.type === "date" && (
        <Form layout="vertical">
          <Form.Item label={config.label} name={config.name}>
            <DatePicker
              name={config.name}
              defaultValue={moment('01/01/2021', dateFormat)}
              format={dateFormat}
              className={config.className}
              disabled={config.disabled}
            />
          </Form.Item>
        </Form>
      )}
      {config.type === "image" && (
        <img src={config.image}></img>
      )}
    </Style>
  );
}
export default input;
