/* eslint-disable react-hooks/rules-of-hooks */
import React from "react";
import { Table } from "antd";
import Styles from "../../styles/components/global/table";
/**
 * @param {dataTable} Parâmetro que recebe um array de objeto para montar os dados
 * @param {columnsTable} Parâmetro que recebe um array para montar as colunas
 * @param {headerTable} Parâmetro boleano para mostrar ou não o header
 * @return <componente a ser renderizado/>
 **/

function MainTable({
  headerTable,
  columnsTable,
  dataTable,
  visbles,
  ColunmModified,
  selectPedidos,
}) {
  const [visibles, setVisibles] = React.useState(false);
  return (
    <Styles>
    <Table
      showHeader={headerTable}
      columns={columnsTable}
      className="table-default"
      dataSource={dataTable}
      pagination={false}
      rowClassName={(row, index) => `row_${index} icons-view rowStyle`}
      onRow={(record, rowIndex) => {
        return {
          onClick: () => visbles(record.key, rowIndex),
        };
      }}
    />
    </Styles>
  );
}
export default MainTable;
