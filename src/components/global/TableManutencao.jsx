/* eslint-disable react-hooks/rules-of-hooks */
import React from "react";
import { Table } from "antd";
import Styles from "../../styles/components/global/tableManutencao";
/**
 * @param {dataTable} Parâmetro que recebe um array de objeto para montar os dados
 * @param {columnsTable} Parâmetro que recebe um array para montar as colunas
 * @param {headerTable} Parâmetro boleano para mostrar ou não o header
 * @return <componente a ser renderizado/>
 **/

function MainTable({
  header_table,
  columns_table,
  data_table,
  visbles,
  Colunm_modified,
  select_pedidos,
}) {
  const [visibles, setVisibles] = React.useState(false);
  return (
    <Styles>
      <Table
        showHeader={header_table}
        columns={columns_table}
        className="table-default"
        dataSource={data_table}
        pagination={false}
        rowClassName={(row, index) => `row_${index} icons-view rowStyle`}
        onRow={(rowIndex) => {
          return {
            onClick: () => visbles(rowIndex)
          }
        }
        }
      />
    </Styles>
  );
}
export default MainTable;
