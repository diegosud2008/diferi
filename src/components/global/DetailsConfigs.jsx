import React from "react";
import { Collapse, Alert, Button, Row, Col, Image, Steps } from "antd";
import { UpOutlined } from "@ant-design/icons";
import FormPerson from "./FormPerson";
import DetailsCss from "../../styles/components/configuracoes/detailsConfigs";
import { Icon } from "../../controller/icon";
import Table from "./Table";

const { Panel } = Collapse;
const { Step } = Steps;
const onClose = (e) => {
  console.log(e, "I was closed.");
};

/**
 * @param {dataTable} Parâmetro que recebe um array de objeto para montar os dados
 * @param {columnsTable} Parâmetro que recebe um array para montar as colunas
 * @param {headerTable} Parâmetro boleano para mostrar ou não o header
 * @param {infosArray} Parâmetro que recebe um array para montagem dos collapses
 * @param {propsDetails} Parâmetro que recebe um objeto para montagem do header
 * @return <componente a ser renderizado/>
 **/

function Details({
  dataTable,
  columnsTable,
  headerTable,
  propsDetails,
  infosArray,
}) {
  return (
    <DetailsCss>
      <div className="">
        <div className="headerDetalhesProdutos">
          <div className="divHeaderDetalhesProdutos">
            <div className="titleHeaderDetalhesProdutos">
              <h1 className="h1HeaderDetalhesProdutos">
                {propsDetails.propsJson.title}
              </h1>
              {propsDetails.propsJson.icon && (
                <div>
                  {Icon(propsDetails.propsJson.icon, "", {
                    width: "13.31px",
                    heigth: "13.31px",
                  })}
                </div>
              )}
            </div>
            {propsDetails.propsJson.btnHeader && (
              <div id="botoesHeaderDetalhesProdutos">
                {propsDetails.propsJson.btnHeader.map((array) => {
                  return (
                    <Button
                      className={array.className}
                      type={array.type}
                      disabled={true}
                    >
                      {array.text}
                    </Button>
                  );
                })}
              </div>
            )}
          </div>
          {propsDetails.propsJson.infoAlert && (
            <div className="alertHeaderDetalhesProdutos">
              <Alert
                message={propsDetails.propsJson.infoAlert.message}
                type={propsDetails.propsJson.infoAlert.type}
                closable
                className={propsDetails.propsJson.infoAlert.class}
                onClose={onClose}
              />
            </div>
          )}
          {propsDetails.propsJson.steps && (
            <div className="timeline">
              <div className="line"></div>
              <Steps size="default" current={1}>
                <Step title="Pendente" />
                <Step title="Pago" />
                <Step title="Faturado" />
                <Step title="Enviado" />
                <Step title="Concluído" />
              </Steps>
            </div>
          )}
        </div>
        <div>
          <Collapse
            className="site-layout-background collapseStyle"
            bordered={false}
            defaultActiveKey={["1"]}
            expandIconPosition="right"
            expandIcon={({ isActive }) => (
              <UpOutlined twoToneColor="#4466AB" rotate={isActive ? 180 : 0} />
            )}
          >
            {infosArray.infosArray.map((temp, index) => {
              return (
                <Panel
                  className="font-20 panel-style"
                  header={temp.collapseTiltle}
                  key={index}
                >
                  <Row>
                    {temp.camposConfig1 && (
                      <Col span={temp.colSpan1}>
                        <div className={temp.colClass1}>
                          {temp.camposConfig1.map((config) => {
                            return <FormPerson config={config} />;
                          })}
                        </div>
                      </Col>
                    )}
                  </Row>
                </Panel>
              );
            })}
          </Collapse>
        </div>
      </div>
    </DetailsCss>
  );
}
export default Details;
