import React, { useEffect, useState } from "react";
import { Collapse, Alert, Button, Row, Col, Image, Steps } from "antd";
import { UpOutlined } from "@ant-design/icons";
import FormPerson from "./FormPerson";
import DetailsCss from "../../styles/components/pedidos/details";
import { Icon } from "../../controller/icon";
import Table from "./Table";
 
const { Panel } = Collapse;
const { Step } = Steps;
const onClose = (e) => {
  console.log(e, "I was closed.");
};
/**
 * @param {dataTable} Parâmetro que recebe um array de objeto para montar os dados
 * @param {columnsTable} Parâmetro que recebe um array para montar as colunas
 * @param {headerTable} Parâmetro boleano para mostrar ou não o header
 * @param {infosArray} Parâmetro que recebe um array para montagem dos collapses
 * @param {propsJson} Parâmetro que recebe um objeto para montagem do header
 * @return <componente a ser renderizado/>
 **/

function Details({
  dataTable,
  columnsTable,
  headerTable,
  propsJson,
  infosArray,
}) {
  const [state, setState] = useState("font-20 panel-style")
  function changeButton() {
    setState('invert-button');
  }
  return (
    <DetailsCss>
      <div className="">
        <div className="headerDetalhesProdutos">
          <div className="divHeaderDetalhesProdutos">
            <div className="titleHeaderDetalhesProdutos">
              <h1 className="h1HeaderDetalhesProdutos">
                {propsJson.propsJson.title}
              </h1>
              {propsJson.propsJson.icon && (
                <div>
                  {Icon(propsJson.propsJson.icon, "", {
                    width: "13.31px",
                    heigth: "13.31px",
                  })}
                </div>
              )}
            </div>
            {propsJson.propsJson.btnHeader && (
              <div id="botoesHeaderDetalhesProdutos">
                {propsJson.propsJson.btnHeader.map((array) => {
                  return (
                    <Button
                      className={array.className}
                      type={array.type}
                      disabled={true}
                    >
                      {array.text}
                    </Button>
                  );
                })}
              </div>
            )}
          </div>
          {propsJson.propsJson.infoAlert && (
            <div className="alertHeaderDetalhesProdutos">
              <Alert
                message={propsJson.propsJson.infoAlert.message}
                type={propsJson.propsJson.infoAlert.type}
                closable
                className={propsJson.propsJson.infoAlert.class}
                onClose={onClose}
              />
            </div>
          )}
          {propsJson.propsJson.steps && (
            <div className="timeline">
              <Steps size="default" current={1}>
              <div className="line"></div>
                <Step title="Pendente" />
                <Step title="Pago" />
                <Step title="Faturado" />
                <Step title="Enviado" />
                <Step title="Concluído" />
              </Steps>
            </div>
          )}
        </div>
        <div>
          <Collapse
            className="site-layout-background collapseStyle"
            bordered={false}
            defaultActiveKey={["1"]}
            expandIconPosition="right"
            expandIcon={({ isActive }) => (
              <UpOutlined twoToneColor="#4466AB" rotate={isActive ? 0 : 180} />
            )}
          >
            {infosArray.infosArray.map((temp, index) => {
              return (
                <Panel
                  className={state}
                  header={temp.collapseTiltle}
                  key={index}
                >
                  <Row>
                    {temp.camposConfig1 && (
                      <Col span={temp.colSpan1}>
                        <div className={temp.colClass1}>
                          {temp.camposConfig1.map((config) => {
                            return <FormPerson config={config} />;
                          })}
                        </div>
                      </Col>
                      
                    )}
                    {temp.img && (
                      <div className="d-flex">
                        {temp.camposConfig1.map((config) => {
                          var labelImage = config.label.split(" ");
                          return (
                            <Col span={config.span}>
                              <div className="box-image">
                                <div className={config.class}>
                                  <div className={config.iconClass}>
                                    <div className="margin-icon">
                                      {Icon(config.icon, "", {
                                        width: "18px",
                                        heigth: "18px",
                                      })}
                                    </div>
                                  </div>
                                  <Image
                                    src={config.image}
                                    preview={config.preview}
                                  />
                                </div>
                                <div className="labelStyle">
                                  {labelImage[0]}
                                  <strong> {labelImage[1]} </strong>
                                  {labelImage[2]} {labelImage[3]}{" "}
                                  {labelImage[4]}
                                </div>
                              </div>
                            </Col>
                          );
                        })}
                      </div>
                    )}
                    {temp.table && (
                      <div className="d-flex w-100">
                        <Col span="24">
                          <Table
                            dataTable={dataTable}
                            columnsTable={columnsTable}
                            headerTable={headerTable}
                          />
                        </Col>
                      </div>
                    )}
                    {temp.camposConfig2 && (
                      <Col span={temp.colSpan2}>
                        <div className={temp.colClass2}>
                          {temp.camposConfig2.map((config) => {
                            return <FormPerson config={config} />;
                          })}
                        </div>
                      </Col>
                    )}
                    {temp.camposConfig3 && (
                      <Col span={temp.colSpan3}>
                        <div className={temp.colClass3}>
                          {temp.camposConfig3.map((config) => {
                            return <FormPerson config={config} />;
                          })}
                        </div>
                      </Col>
                    )}
                  </Row>
                </Panel>
              );
            })}
          </Collapse>
        </div>
      </div>
    </DetailsCss>
  );
}
export default Details;
