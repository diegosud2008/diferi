import React, { useEffect } from "react";
import { Tree } from "antd";
import StyleTree from "../../styles/components/global/tree";
import { Icon } from "../../controller/icon";

function TreeDefault({ treeData, onSelect, onCheck, onExpand }) {

  const renderTreeNodes = (data, first = true) =>
    data.map((item, index) => {
      if (item.children) {
        return (
          <Tree.TreeNode style={{ fontWeight: !first ? "normal" : "bold" }} title={(<div className="parent-div"><span>{item.title}</span><span className={`total-items-${item.key}`}>119 itens</span></div>)} key={item.key} dataRef={item}>
            {renderTreeNodes(item.children, false)}
          </Tree.TreeNode>
        )
      }
      if (item.marketplaces) {
        return (
          <Tree.TreeNode style={{ fontWeight: !first ? "normal" : "bold" }} title={<div className="last-child-tree"><div>{item.title}</div><div className={`mkps`}>{item.marketplaces.map(marketplaceIcon => (Icon(marketplaceIcon, "", { width: "24px", heigth: "20px" })))}</div></div>} key={item.key} dataRef={item} />
        )
      }
      return <Tree.TreeNode style={{ fontWeight: !first ? "normal" : "bold" }} key={item.key} {...item} />
    })
  return (
    <StyleTree>
      <Tree
        checkable
        switcherIcon={Icon("arrowDefault", "", {
          width: "16x",
        })}
        onSelect={onSelect}
        onCheck={onCheck}
        onExpand={onExpand}
      >
        {renderTreeNodes(treeData.treeDataInfo)}
      </Tree>
    </StyleTree>
  );
}
export default TreeDefault;
