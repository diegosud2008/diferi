import React, { useState } from "react";
import { Drawer, Collapse, Descriptions } from "antd";
import Link from "next/link";
import { Icon } from "../../controller/icon";
import { UpOutlined } from "@ant-design/icons";
import {Styles, StylesDrawer} from "../../styles/components/global/drawer";

const { Panel } = Collapse;

function MainDrawer({ vesibles, onClose, drawerJson }) {
  const title = () => {
    return (
      <Styles>
        <div className="title">
          <div>Categoria <strong>{drawerJson.categoria}</strong></div>
          {drawerJson.produtos && (
            <div className="div-header">
              {" "}
              <div className="div-text">
                <p className="titleProdutos">
                  <strong>Ventisol</strong> Aquecedor doméstico halógeno AH-01
                  127 V premium
                </p>
              </div>
              <div className="div-img">
                {drawerJson.image && (
                  <div className="div-int-img">
                    <img className="styleImage" src={drawerJson.image}></img>
                  </div>
                )}{" "}
              </div>
            </div>
          )}
        </div>
      </Styles>
    );
  };
  return (
    <StylesDrawer>
      <Drawer
        title={title()}
        style={{ position: "absolute" }}
        width={"100%"}
        placement="right"
        visible={vesibles}
        getContainer={false}
        closable={true}
        headerStyle={{
          background: "#F3F5F9",
          position: "relative",
        }}
        bodyStyle={{ width: "89%" }}
        onClose={onClose}
      >
        {/* <div className="close" onClick={ onClose }>{ Icon('closeIcon') }</div> */}
        <Collapse
          expandIconPosition="right"
          expandIcon={({ isActive }) => (
            <UpOutlined twoToneColor="#4466AB" rotate={isActive ? 180 : 0} />
          )}
        >
          {drawerJson.dados.map((json, index) => {
            return (
              <Panel
                header={json.collapseTitle}
                key={index}
                extra={Icon("linkOutlineGray", "", {
                  width: "16px",
                  heigth: "16px",
                })}
              >
                {drawerJson.dados[index].categoriasJson.map((json, index) => {
                  if (json.vinculo) {
                    return (
                      <div className="d-flex justify-content-between text-align-center pt-1">
                        <div className="spamLabel">
                          {json.icon &&
                            Icon(json.icon, "", {
                              width: "24px",
                              heigth: "24px",
                            })}
                          <spam className={`${json.classLabel}`}>
                            {json.label}
                          </spam>
                        </div>
                        {json.value &&
                          <div className="spamValue">
                            <spam className={`${json.classValue}`}>
                              {json.value}
                            </spam>
                          </div>}
                        <div className="spamValue">
                          {json.vinculado &&
                            <span className="spamBold">Vinculado</span>
                          }
                          {!json.vinculado &&
                            <span className="spamBold inactive">Sem vinculo</span>
                          }
                          <Link href="/hubba/cadastros/vinculocategorias">
                            <span style={{marginRight:"23px"}}>{Icon("linkOutlineGray", "", {
                              width: "12px",
                              heigth: "12px",
                              cursor:"pointer"
                            })}</span>
                          </Link>
                        </div>

                      </div>
                    )
                  } else {
                    return (
                      <div className="d-flex justify-content-between text-align-center pt-1">
                        <div className="spamLabel">
                          {json.icon &&
                            Icon(json.icon, "", {
                              width: "24px",
                              heigth: "24px",
                            })}
                          <spam className={`${json.classLabel}`}>
                            {json.label}
                          </spam>
                        </div>
                        {json.value &&
                          <div className="spamValue">
                            <spam className={`${json.classValue}`}>
                              {json.value}
                            </spam>
                          </div>}
                      </div>
                    )
                  }
                })}
              </Panel>
            );
          })}
        </Collapse>
      </Drawer>
    </StylesDrawer>
  );
}
export default MainDrawer;
