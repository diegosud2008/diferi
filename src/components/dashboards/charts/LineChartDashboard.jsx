import React from "react";
import { ResponsiveContainer, Legend, LineChart, XAxis, YAxis, Tooltip, Line, CartesianGrid } from 'recharts';
import Link from "next/dist/client/link";
import Styles from '../../../styles/components/dashboards/charts/lineChartDashboard';
import moment from 'moment';

function LineChartDashboard({ data }) {
    var lastTick = ""
    function tickFormatterDate(date){
        if (moment(date, "YYYY-MM-DD HH:mm:ss").format("MMMM") != lastTick){
            lastTick = moment(date, "YYYY-MM-DD HH:mm:ss").format("MMMM")
            return moment(date, "YYYY-MM-DD HH:mm:ss").format("MMMM")
        }else{
            return ""
        }
    }
    return (
        <Styles>
            <Link href="/hubba/pedidos">
                <div className="lineChartDashboard">
                    <h3>{data.title}</h3>
                    <ResponsiveContainer width="95%" height={360}>
                        <LineChart data={data.data}>
                            <CartesianGrid strokeDasharray="1 5" stroke="#2A2A2C" vertical={false} />
                            <XAxis dataKey="name" axisLine={false} tickLine={false} tickFormatter={tickFormatterDate}/>
                            <YAxis axisLine={false} tickLine={false} tick={{ stroke: '#000', strokeWidth: 1 }} tickMargin="10" />
                            <Tooltip labelFormatter={date => moment(date, "YYYY-MM-DD HH:mm:ss").format("DD-MM-YYYY")} />
                            <Legend verticalAlign="top" align="right" height={36} iconType="circle" formatter={text => <span style={{ color: "#000" }}>{text}</span>} />
                            {data.ref.map((entry, index) => (
                                <Line type="linear" dataKey={entry.name} strokeWidth={5} stroke={entry.color} dot={{ strokeWidth: 4, r: 5 }} />
                            ))}
                        </LineChart>
                    </ResponsiveContainer>
                </div>
            </Link>
        </Styles>
    );
}

export default LineChartDashboard;
