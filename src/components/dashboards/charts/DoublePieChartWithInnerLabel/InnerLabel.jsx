import React from "react";

export default function CustomLabel({ viewBox, value1, value2 }) {
    const { cx, cy } = viewBox;
    return (
        <>
            <text x={cx} y={cy} fill="#3d405c" className="recharts-text recharts-label" textAnchor="middle" dominantBaseline="central">
                <tspan alignmentBaseline="middle" fontSize="42">{value1}</tspan>

            </text>
            <text x={cx} y={cy + 30} fill="#3d405c" className="recharts-text recharts-label" textAnchor="middle" dominantBaseline="central">
                <tspan alignmentBaseline="middle" fontSize="11">{value2}</tspan>
            </text>
        </>
    )
}