import React from "react";
import { PieChart, Pie, Cell, ResponsiveContainer, Legend, Label } from 'recharts';
import Link from "next/dist/client/link";
import InnerLabel from "./InnerLabel"
import Styles from '../../../../styles/components/dashboards/charts/doublePieChartWithInnerLabelDashboard'

function DoublePieChartWithInnerLabelDashboard({ data1, data2 }) {

  return (
    <Styles>
      <div className="doublePieChartWithInnerLabelDashboard">
      <Link href="/hubba/pedidos">
        <div>
          <h3>Pedidos</h3>
          <ResponsiveContainer width="50%" height={200} minWidth={567}>
            <PieChart>
              <Pie
                data={data1.data}
                cx="18%"
                cy={100}
                innerRadius={60}
                outerRadius={90}
                fill="#8884d8"
                paddingAngle={1}
                dataKey="value"
              >
                {data1.data.map((entry, index) => (
                  <Cell key={`cell-${index}`} fill={entry.color} />
                ))}
                <Label width={30} position="center"
                  content={<InnerLabel value1={data1.total} value2="mil" />}>
                </Label>
              </Pie>
              <Legend verticalAlign="middle" aling="right" layout="vertical" width="50%" iconType="square" wrapperStyle={{ paddingLeft: "90px" }} />
            </PieChart>
          </ResponsiveContainer>
        </div>
      </Link>
      <Link href="/hubba/produtos">
        <div>
          <h3>Anúncios</h3>
          <ResponsiveContainer width="50%" height={200} minWidth={567}>
            <PieChart>
              <Pie
                data={data2.data}
                cx="18%"
                cy={100}
                innerRadius={60}
                outerRadius={90}
                fill="#8884d8"
                paddingAngle={1}
                dataKey="value"
              >
                {data2.data.map((entry, index) => (
                  <Cell key={`cell-${index}`} fill={entry.color} />
                ))}
                <Label width={30} position="center"
                  content={<InnerLabel value1={data2.total} value2="mil" />}>
                </Label>
              </Pie>
              <Legend verticalAlign="middle" aling="right" layout="vertical" width="50%" iconType="square" wrapperStyle={{ paddingLeft: "90px" }} />
            </PieChart>
          </ResponsiveContainer>
        </div>
      </Link>
    </div>
    </Styles>
  );
}

export default DoublePieChartWithInnerLabelDashboard;
