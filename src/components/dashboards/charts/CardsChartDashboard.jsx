import React from "react";
import Link from "next/dist/client/link";
import Styles from '../../../styles/components/dashboards/charts/cardsChartDashboard'

function CardsChartDashboard({ data }) {

    return (
        <Styles>
            <div className="cardsChartDashboard">
                {data.map((el) => {
                    if (el.clickable) {
                        return (
                            <Link href={el.to}>
                                <div className="cardConector" style={{ backgroundColor: el.color, cursor: "pointer" }}>
                                    <h3>{el.title}</h3>
                                    <div><span>{el.value}</span></div>
                                </div>
                            </Link>
                        )
                    } else {
                        return (
                            <div className="cardConector" style={{ backgroundColor: el.color }}>
                                <h3>{el.title}</h3>
                                <div><span>{el.value}</span></div>
                            </div>
                        )
                    }
                })}
            </div>
        </Styles>

    );
}

export default CardsChartDashboard;