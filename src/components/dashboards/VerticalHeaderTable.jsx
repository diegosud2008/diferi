import React from "react";
import Styles from '../../styles/components/dashboards/verticalHeaderTable'

function VerticalHeaderTable({ data }) {

    return (
        <Styles>
            <div className="verticalHeaderTable">
                <table>
                    {data.map(el => (
                        <tr>
                            <td>
                                {el.title}
                            </td>
                            <td>
                                {el.value}
                            </td>
                        </tr>
                    ))}
                </table>
            </div>
        </Styles>
    );
}

export default VerticalHeaderTable;
