import React from "react";
import Styles from '../../styles/components/dashboards/highlightProduct'

function HighlightProduct({ data }) {

    return (
        <Styles>
            <div className="highlightProduct">
              <img src={data.imageURL} alt="Highlight product image"/>
              <span>{data.quantity}</span>
              <span>{data.productName}</span>
              <span>{data.productBrand}</span>  
            </div>
        </Styles>
    );
}

export default HighlightProduct;
